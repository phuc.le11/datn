/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Linking,
  Dimensions,
  AsyncStorage,
  Alert,
  Platform,
} from 'react-native';
import moment from 'moment';
import MapView from 'react-native-maps';
import ic_address from './images/address.png';
import ic_text from './images/text.png';
import ic_phone from './images/phone.png';
import ic_time from './images/ic-time.png';

import firebase from 'react-native-firebase';

const width = Dimensions.get('window').width;

const Detail = props => {
  const [item, setItem] = useState(props.navigation.getParam('item') || {});

  const updateDonation = async () => {
    const volunteerId = await AsyncStorage.getItem('volunteerId');
    console.log('volunteerId', item.id);

    if (volunteerId !== null && item.data) {
      let updateStatus = '';
      const status = item.data.status;
      if (status === 'pending') {
        updateStatus = 'processing';
      } else if (status === 'processing') {
        updateStatus = 'pending';
      } else {
        return;
      }
      const ref = firebase
        .firestore()
        .collection('donations')
        .doc(item.id);

      firebase
        .firestore()
        .runTransaction(async donation => {
          const newDoc = {
            ...item.data,
            status: updateStatus,
            volunteerId: volunteerId,
          };
          donation.update(ref, newDoc);
          return newDoc;
        })
        .then(updatedItem => {
          setItem({ id: item.id, data: { ...updatedItem } });
        })
        .catch(error => {});
    }
  };

  const openMap = () => {
    const query = `${item.data.lat},${item.data.lng}`;
    Linking.openURL(`https://www.google.com/maps/search/?api=1&query=${query}`);
  };

  const callNumber = phone => {
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Phone number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };

  const renderInfo = () => (
    <View
      style={{
        flexDirection: 'row',
        margin: 10,
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
        justifyContent: 'space-between',
      }}>
      <View style={{ width: width * 0.5 }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 5,
          }}>
          <Image
            source={ic_address}
            style={{ width: 18, height: 18, marginRight: 10 }}
          />
          <Text>{(item.data && item.data.address) || ''}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 5,
          }}>
          <Image
            source={ic_text}
            style={{ width: 18, height: 18, marginRight: 10 }}
          />
          <Text>{(item.data && item.data.description) || ''}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 5,
          }}>
          <Image
            source={ic_phone}
            style={{ width: 18, height: 18, marginRight: 10 }}
          />
          <Text
            {...item.data.phoneNumber && {
              onPress: () => callNumber(item.data.phoneNumber),
            }}
          >
            {(item.data && item.data.phoneNumber) || 'No phone number'}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 5,
          }}>
          <Image
            source={ic_time}
            style={{ width: 18, height: 18, marginRight: 10 }}
          />
          <Text>
            {`Want to pick up on ${(item.data &&
              moment(item.data.donationTime).format(
                'MMM Do YYYY, h:mm a',
              )) ||
              ''}`}
          </Text>
        </View>
      </View>
      <Text
        style={{
          height: 40,
          color: '#fff',
          fontWeight: 'bold',
          paddingVertical: 10,
          paddingHorizontal: 15,
          borderRadius: 5,
          backgroundColor:
            item.data && item.data.status === 'pending' ? '#f5522d' : '#43a047',
        }}>
        {(item.data && item.data.status) || ''}
      </Text>
    </View>
  );

  const renderImages = () =>
    item.data &&
    item.data.imageURL &&
    item.data.imageURL.length > 0 && (
      <View
        style={{
          padding: 10,
          margin: 10,
          marginTop: 0,
          backgroundColor: '#fff',
          borderRadius: 10,
        }}>
        <ScrollView horizontal={true}>
          {item.data.imageURL &&
            item.data.imageURL.map((uri, index) => (
              <Image
                source={{ uri }}
                style={{ width: 100, height: 100, marginRight: 10 }}
                key={`${index}`}
              />
            ))}
        </ScrollView>
      </View>
    );

  const renderMap = () => (
    <View style={{ flex: 7, backgroundColor: '#fff', paddingBottom: 10 }}>
      <Text style={{ fontSize: 18, fontWeight: 'bold', margin: 10 }}>
        Location
      </Text>
      <View
        style={{
          width: '95%',
          alignSelf: 'center',
          height: 380,
        }}>
        {item.data && item.data.lat && item.data.lng && (
          <MapView
            style={{ flex: 1 }}
            region={{
              latitude: parseFloat(item.data.lat),
              longitude: parseFloat(item.data.lng),
              latitudeDelta: 0.09922,
              longitudeDelta: 0.09421,
            }}>
            <MapView.Marker
              coordinate={{
                latitude: parseFloat(item.data.lat),
                longitude: parseFloat(item.data.lng),
              }}
              title={item.data.giverName}
              onPress={openMap}
            />
          </MapView>
        )}
      </View>
    </View>
  );

  const renderButton = () => (
    <View
      style={{
        position: 'absolute',
        width: '100%',
        bottom: 10,
      }}>
      {item.data.status !== 'success' && (
        <TouchableOpacity
          onPress={updateDonation}
          style={{
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            width: 200,
            height: 40,
            marginVertical: 10,
            borderRadius: 20,
            elevation: 1,
            backgroundColor: '#3fbdab',
          }}>
          <Text style={{ color: '#fff', fontWeight: 'bold' }}>
            {item.data.status === 'pending' ? 'TAKE JOB' : 'CANCEL'}
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );

  return (
    <View style={{ flex: 1, backgroundColor: '#efefef' }}>
      <ScrollView contentContainerStyle={{ paddingBottom: 30 }}>
        {renderInfo()}
        {/* {renderImages()} */}
        {renderMap()}
      </ScrollView>
      {renderButton()}
    </View>
  );
};

Detail.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title'),
  headerStyle: {
    backgroundColor: '#3fbdab',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
});

export default Detail;
