import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Alert,
  AsyncStorage,
  TouchableHighlight,
} from 'react-native';
import firebase from 'react-native-firebase';
import { addFirebaseData } from './firebase';

class SignUp extends React.Component {
  state = {
    email: 'Name@gmail.com',
    password: 'password',
    fullname: 'Full Name',
    phone: '0935000001',
    address: 'Address',
    errorMessage: null,
  };

  handleSignUp = () => {
    const { email, password, fullname, phone, address } = this.state;

    if (email === '') {
      return Alert.alert('Invalid', 'Please enter email', [
        { text: 'Ok', onPress: () => {} },
      ]);
    }

    if (password === '') {
      return Alert.alert('Invalid', 'Please enter password', [
        { text: 'Ok', onPress: () => {} },
      ]);
    }

    if (fullname === '') {
      return Alert.alert('Invalid', 'Please enter your full name', [
        { text: 'Ok', onPress: () => {} },
      ]);
    }

    if (phone === '') {
      return Alert.alert('Invalid', 'Please enter your phone number', [
        { text: 'Ok', onPress: () => {} },
      ]);
    }

    if (address === '') {
      return Alert.alert('Invalid', 'Please enter your address', [
        { text: 'Ok', onPress: () => {} },
      ]);
    }

    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(res => {
        this.addUser();
      })
      .catch(error => {
        console.log('error', error);
        this.setState({ errorMessage: error.message });
      });
  };

  addUser = () => {
    const { email, fullname, phone, address } = this.state;
    const data = {
      email,
      address,
      name: fullname,
      phoneNumber: phone,
    };
    addFirebaseData('volunteers', data)
      .then(async res => {
        await AsyncStorage.setItem('volunteerId', res.id);
      })
      .catch(error => {
        console.log('error', error);
      });
  };

  goToLogin = () => {
    this.props.navigation.navigate('Login');
  };

  render() {
    const { email, password, phone, address, fullname } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.textHeader}>Sign Up</Text>
        {this.state.errorMessage && (
          <Text style={{ color: 'red' }}>{this.state.errorMessage}</Text>
        )}
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="Email"
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            onChangeText={email => this.setState({ email })}
            value={email}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Password"
            autoCapitalize="none"
            secureTextEntry
            underlineColorAndroid="transparent"
            style={styles.inputs}
            onChangeText={password => this.setState({ password })}
            value={password}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Full name"
            underlineColorAndroid="transparent"
            style={styles.inputs}
            onChangeText={fullname => this.setState({ fullname })}
            value={fullname}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Address"
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            style={styles.inputs}
            onChangeText={address => this.setState({ address })}
            value={address}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Phone number"
            keyboardType="number-pad"
            underlineColorAndroid="transparent"
            style={styles.inputs}
            onChangeText={phone => this.setState({ phone })}
            value={phone}
          />
        </View>
        <TouchableHighlight
          underlayColor="#47e4c8"
          style={[styles.buttonContainer, styles.loginButton]}
          onPress={this.handleSignUp}>
          <Text style={styles.loginText}>Sign up</Text>
        </TouchableHighlight>
        <Text>
          Already have an account?{' '}
          <Text style={styles.text} onPress={this.goToLogin}>
            Login
          </Text>
        </Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#11e6c0',
  },
  textHeader: {
    marginBottom: 20,
    fontSize: 20,
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#DCDCDC',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#DCDCDC',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: '#11e6c0',
  },
  loginText: {
    color: 'white',
  },
});

export default SignUp;
