/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  ActivityIndicator,
  StatusBar,
  Dimensions,
  Button,
  TextInput,
} from 'react-native';
import moment from 'moment';
import firebase from 'react-native-firebase';
import ic_address from './images/address.png';
import ic_text from './images/text.png';
import ic_time from './images/ic-time.png';
// import ic_avatar from './images/avatar.png';
import ic_list from './images/list.png';
import ic_search from './images/ic-search.png';
import ic_userList from './images/ic-user.png';
import ic_user from './images/user.png';

const width = Dimensions.get('window').width;

const ListView = props => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [filtersData, setFilterData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const ref = firebase
        .firestore()
        .collection('donations')
        .where('status', '==', 'pending');

      ref.onSnapshot({ includeMetadataChanges: true }, onSnapshot => {
        let arr = [];
        console.log('onSnapshot', onSnapshot);
        onSnapshot.forEach(doc => {
          arr.push({ id: doc.id, data: doc.data() });
        });

        setData(arr);
        setFilterData(arr);
        setLoading(false);
      });
    };

    setLoading(true);
    fetchData();
  }, []);

  const goToDetail = item => () => {
    props.navigation.navigate('Detail', { item, title: item.data.giverName });
  };

  const onFilters = text => {
    if (!text) {
      setFilterData(data);
      return;
    }
    const tempData = data.filter(elm => {
      let address = elm.data.address;
      address = address.toLowerCase();
      return address.includes(text.toLowerCase());
    });
    setFilterData(tempData);
  };

  const item = ({ item }) => (
    <TouchableOpacity
      style={{
        flex: 1,
        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'space-between',
        margin: 10,
        padding: 10,
        borderRadius: 10,
        borderWidth: 1,
        backgroundColor: '#fff',
        borderColor: '#fff',
      }}
      onPress={goToDetail(item)}>
      {/* <Image
        source={ic_avatar}
        style={{ width: 30, height: 30, borderRadius: 15, marginRight: 15 }}
      /> */}
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: width * 0.5,
        }}>
        <View>
          <View
            style={{
              width: 150,
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 5,
            }}>
            <Image
              source={ic_userList}
              style={{
                width: 18,
                height: 18,
                marginRight: 10,
                marginBottom: 3,
              }}
            />
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 4 }}>
              {item.data.giverName}
            </Text>
          </View>
          <View
            style={{
              width: 150,
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 5,
            }}>
            <Image
              source={ic_address}
              style={{ width: 18, height: 18, marginRight: 10 }}
            />
            <Text>{item.data.address}</Text>
          </View>
          <View
            style={{
              width: 150,
              flexDirection: 'row',
              alignItems: 'center',
              marginVertical: 5,
            }}>
            <Image
              source={ic_text}
              style={{ width: 18, height: 18, marginRight: 10 }}
            />
            <Text numberOfLines={2} ellipsizeMode={'tail'}>
              {item.data.description}
            </Text>
          </View>
          <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 5,
          }}>
          <Image
            source={ic_time}
            style={{ width: 18, height: 18, marginRight: 10 }}
          />
          <Text>
            {`Want to pick up on ${(item.data &&
              moment(item.data.donationTime).format(
                'MMM Do YYYY, h:mm a',
              )) ||
              ''}`}
          </Text>
        </View>
        </View>
      </View>
      <View
        style={{
          height: 40,
          paddingHorizontal: 10,
          backgroundColor: '#f5522d',
          borderRadius: 5,
          justifyContent: 'center',
        }}>
        <Text style={{ color: '#fff', fontWeight: 'bold' }}>
          {moment.unix(item.data.createdDate).fromNow()}
        </Text>
      </View>
    </TouchableOpacity>
  );

  const listEmptyComponent = () => (
    <View style={{ flex: 1, alignItems: 'center', marginTop: 40 }}>
      <Text style={{ fontWeight: 'bold', fontSize: 18 }}>
        No Pending Donations at the moment
      </Text>
    </View>
  );

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator color="#3fbdab" size={'large'} />
      </View>
    );
  }

  return (
    <View style={{ flex: 1, backgroundColor: '#efefef' }}>
      <StatusBar backgroundColor="#00701a" barStyle="light-content" />
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: '#fff',
          margin: 12,
          borderRadius: 5,
          paddingHorizontal: 12,
        }}>
        <Image
          source={ic_search}
          style={{ width: 24, height: 24, marginRight: 12 }}
        />
        <TextInput
          style={{ flex: 1, fontSize: 17 }}
          placeholder="Search"
          onChangeText={onFilters}
        />
      </View>
      <FlatList
        renderItem={item}
        data={filtersData}
        ListEmptyComponent={listEmptyComponent}
        keyExtractor={(item, index) => `${index}`}
      />
    </View>
  );
};

ListView.navigationOptions = ({ navigation }) => ({
  title: 'Pending Donations',
  headerStyle: {
    backgroundColor: '#3fbdab',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
  headerRight: (
    <View style={{ flexDirection: 'row' }}>
      <TouchableOpacity
        style={{ margin: 10 }}
        onPress={() => navigation.navigate('MyList')}>
        <Image source={ic_list} style={{ width: 24, height: 24 }} />
      </TouchableOpacity>
      <TouchableOpacity
        style={{ margin: 10 }}
        onPress={() => navigation.navigate('Profile')}>
        <Image source={ic_user} style={{ width: 24, height: 24 }} />
      </TouchableOpacity>
    </View>
  ),
});

export default ListView;
