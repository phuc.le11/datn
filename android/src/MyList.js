/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
  StyleSheet,
  AsyncStorage,
  TouchableHighlight,
} from 'react-native';
import moment from 'moment'
import firebase from 'react-native-firebase';
import ic_address from './images/address.png';
import ic_text from './images/text.png';
import ic_avatar from './images/avatar.png';
import ic_time from './images/ic-time.png';

const MyList = props => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const jobType = props.navigation.getParam('type');
  console.log('jobType', jobType);

  useEffect(() => {
    const fetchData = async () => {
      const volunteerId = await AsyncStorage.getItem('volunteerId');
      const ref = firebase
        .firestore()
        .collection('donations')
        .where('volunteerId', '==', volunteerId);
      ref.onSnapshot({ includeMetadataChanges: true }, onSnapshot => {
        let arr = [];
        onSnapshot.forEach(doc => {
          if (doc.data().status !== 'pending') {
            arr.push({
              id: doc.id,
              data: doc.data(),
            });
          }
        });
        console.log('arr', arr);
        const jobs = arr.filter(elm => elm.data.status === jobType);
        setData(jobs);
        setLoading(false);
      });
    };

    setLoading(true);
    fetchData();
  }, [jobType]);

  const goToDetail = item => () => {
    props.navigation.navigate('Detail', { item, title: item.data.giverName });
  };

  const Item = ({ item }) => (
    <TouchableHighlight onPress={goToDetail(item)} underlayColor="#fff">
      <View style={styles.itemWrap}>
        {/* <Image source={ic_avatar} style={styles.avatar} /> */}
        <View style={styles.fill}>
          <Text style={styles.label}>{item.data.giverName}</Text>
          <View style={styles.subView}>
            <Image source={ic_address} style={styles.icon} />
            <Text>{item.data.address}</Text>
          </View>
          <View style={styles.subView}>
            <Image source={ic_text} style={styles.icon} />
            <Text>{item.data.description}</Text>
          </View>
          <View style={styles.subView}>
            <Image
              source={ic_time}
              style={styles.icon}
            />
            <Text>
              {`Want to pick up on ${(item.data &&
                moment(item.data.donationTime).format(
                  'MMM Do YYYY, h:mm a',
                )) ||
                ''}`}
            </Text>
          </View>
        </View>
        <View
          style={[
            styles.tag,
            {
              backgroundColor:
                item.data.status === 'success' ? '#43a047' : '#039be5',
            },
          ]}>
          <Text style={{ color: '#fff' }}>{item.data.status}</Text>
        </View>
      </View>
    </TouchableHighlight>
  );

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator color={'#43a047'} size={'large'} />
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        renderItem={Item}
        data={data}
        keyExtractor={(item, index) => `${index}`}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  itemWrap: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#fff',
    padding: 24,
    paddingBottom: 10,
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    marginRight: 16,
  },
  tag: {
    position: 'absolute',
    top: 16,
    right: 16,
    paddingHorizontal: 8,
    paddingVertical: 4,
    width: 100,
    borderRadius: 4,
    alignItems: 'center',
  },
  subView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
  },
  icon: {
    width: 18,
    height: 18,
    marginRight: 10,
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  fill: {
    flex: 1,
    paddingBottom: 16,
    borderBottomWidth: 2,
    borderBottomColor: '#efefef',
  },
});

export default MyList;
