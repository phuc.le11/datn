import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

//
import ListView from '../ListView';
import Detail from '../Detail';
// import MyList from '../MyList';
import Login from '../Login';
import SignUp from '../Signup';
import Splash from '../Splash';
import PendingList from '../MyList';
import SuccessList from '../MyList';
import Profile from '../Profile';

const MyListTabs = createBottomTabNavigator(
  {
    PendingList: {
      screen: PendingList,
      navigationOptions: {
        tabBarLabel: 'Processing Jobs',
      },
      params: {
        type: 'processing',
      },
    },
    SuccessList: {
      screen: SuccessList,
      navigationOptions: {
        tabBarLabel: 'Success Jobs',
      },
      params: {
        type: 'success',
      },
    },
  },
  {
    tabBarOptions: {
      labelStyle: {
        fontSize: 15,
        marginBottom: 12,
        fontWeight: '500',
      },
      showLabel: true,
    },
  },
);

const MyListContainer = createAppContainer(MyListTabs);

const ListStack = createStackNavigator({
  Splash,
  Login,
  SignUp,
  ListView,
  Detail,
  Profile,
  MyList: {
    screen: MyListContainer,
    navigationOptions: {
      headerMode: 'screen',
    },
  },
});

export const Root = createAppContainer(ListStack);
