import firebase from 'react-native-firebase';

export const getFirebaseData = async collection => {
  const snapshot = await firebase
    .firestore()
    .collection(collection)
    .get();
  let responseData = [];
  snapshot.docs.forEach(
    doc =>
      (responseData = [
        ...responseData,
        Object.assign(doc.data(), { id: doc.id }),
      ]),
  );
  return responseData;
};

export const getFirebaseDataFilterMany = async (collection, filter = null) => {
  const snapshot = await firebase
    .firestore()
    .collection(collection)
    .where(filter.field, filter.operator, filter.value)
    .get();

  let responseData = [];
  snapshot.docs.forEach(
    doc =>
      (responseData = [
        ...responseData,
        Object.assign(doc.data(), { id: doc.id }),
      ]),
  );
  return responseData;
};

export const getFirebaseDataFilter = async (collection, filter = null) => {
  const snapshot = await firebase
    .firestore()
    .collection(collection)
    .where(filter.field, filter.operator, filter.value)
    .limit(1)
    .get();
  console.log('snapshot', snapshot.docs[0].id);
  return { data: snapshot.docs[0]._data, id: snapshot.docs[0].id };
};

export const getOne = async (collection, id) => {
  const snapshot = await firebase
    .firestore()
    .doc(`${collection}/${id}`)
    .get();
  return snapshot._data;
};

export const addFirebaseData = async (collection, data) => {
  const ref = firebase.firestore().collection(collection);
  return await ref.add(
    Object.assign(
      {
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      },
      data,
    ),
  );
};

export const updateFirebaseData = async (collection, id, data) => {
  const ref = firebase.firestore().collection(collection);
  return await ref.doc(id).update(data);
};

export const getDocumentId = (userList, userId) => {
  const filterItem = userList.filter(item => item.userId === userId);
  if (filterItem) {
    return filterItem[0].id;
  }
};

export const deleteFirebaseData = async (collection, id) => {
  const ref = firebase.firestore().collection(collection);
  return await ref.doc(id).delete();
};
