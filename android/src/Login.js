import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  AsyncStorage,
  Image,
  TouchableHighlight,
} from 'react-native';
import firebase from 'react-native-firebase';
import { getFirebaseDataFilter } from './firebase';
// import { TouchableHighlight } from 'react-native-gesture-handler';
import imageLogo from './images/avatar.png';

class Login extends React.Component {
  state = {
    email: 'phuc.le@gmail.com',
    password: '123456',
    errorMessage: null,
  };

  handleLogin = () => {
    const { email, password } = this.state;
    if (email && password) {
      console.log('asdasd wtf')
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(res => {
          console.log('signInWithEmailAndPassword', res.user.email);
          this.getUserWithMail(res.user.email);
        })
        .catch(error => this.setState({ errorMessage: error.message }));
    }
  };

  getUserWithMail = async email => {
    const { id } = await getFirebaseDataFilter('volunteers', {
      field: 'email',
      operator: '==',
      value: email,
    });
    await AsyncStorage.setItem('volunteerId', id);
  };

  goToSignUp = () => {
    this.props.navigation.navigate('SignUp');
  };

  render() {
    return (
      <View style={styles.container}>
        {/* <Text>Login</Text> */}
        <Image source={imageLogo} style={styles.logo} />
        {this.state.errorMessage && (
          <Text
            style={{
              color: 'red',
              width: 250,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {this.state.errorMessage}
          </Text>
        )}
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            autoCapitalize="none"
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid="transparent"
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            secureTextEntry
            style={styles.inputs}
            autoCapitalize="none"
            underlineColorAndroid="transparent"
            placeholder="Password"
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />
        </View>
        <View>
          <TouchableHighlight
            underlayColor="#47e4c8"
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={this.handleLogin}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableHighlight>
        </View>
        <Text>
          Don't have an account?{' '}
          <Text style={styles.text} onPress={this.goToSignUp}>
            Sign up
          </Text>
        </Text>
        <View>
          <TouchableHighlight style={styles.buttonContainer}>
            <Text style={{ color: '#ffff' }}>Sign up</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    flex: 1,
    width: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  text: {
    color: '#11e6c0',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#DCDCDC',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#DCDCDC',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: '#11e6c0',
  },
  loginText: {
    color: 'white',
  },
});

export default Login;
