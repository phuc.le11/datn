import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';
import firebase from 'react-native-firebase';

class Profile extends Component {
  static navigationOptions = {
    title: 'Profile',
  };

  render() {
    return (
      <View style={{ padding: 20 }}>
        <Button
          title={'Logout'}
          onPress={() => {
            firebase
              .auth()
              .signOut()
              .then(() => {})
              .catch();
          }}
        />
      </View>
    );
  }
}

export default Profile;
