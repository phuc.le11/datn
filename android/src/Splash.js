import React from 'react';
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native';
import firebase from 'react-native-firebase';
import { StackActions, NavigationActions } from 'react-navigation';

class Splash extends React.Component {
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      console.log('user', user);
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: user ? 'ListView' : 'Login',
          }),
        ],
      });
      this.props.navigation.dispatch(resetAction);
      //   this.props.navigation.navigate(user ? 'ListView' : 'Login');
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Splash;
