// import firebase from 'react-native-firebase';

// firebase.initializeApp({
//   project_number: '376821161937',
//   firebase_url: 'https://project-204579062661592825.firebaseio.com',
//   project_id: 'project-204579062661592825',
//   storage_bucket: 'project-204579062661592825.appspot.com',
// });

// async function onSignIn() {
//   // Get the users ID
//   const uid = auth().currentUser.uid;

//   // Create a reference
//   const ref = database().ref(`/users/${uid}`);

//   // Fetch the data snapshot
//   const snapshot = await ref.once('value');

//   console.log('User data: ', snapshot.val());
// }

import React, { useState, useEffect } from 'react';
import { Text, FlatList } from 'react-native';

function Games() {
  const [loading, setLoading] = useState(true);
  const [games, setGames] = useState([]);

  // Handle snapshot response
  function onSnapshot(snapshot) {
    const list = [];

    // Create our own array of games in order
    snapshot.forEach(game => {
      list.push({
        key: game.id, // Add custom key for FlatList usage
        ...game,
      });
    });

    setGames(list);
    setLoading(false);
  }

  useEffect(() => {
    // Create reference
    // const ref = database().ref(`/games`);
    // ref.once('value', onSnapshot);
  }, []);

  if (loading) {
    return <Text>Loading games...</Text>;
  }

  return (
    <FlatList
      data={games}
      renderItem={({ item }) => <Text>{item.name}</Text>}
    />
  );
}

export default Games;
