import withAuthen from './withAuthen';
import withFetchDataById from './withFetchDataById';

export { withAuthen, withFetchDataById };
