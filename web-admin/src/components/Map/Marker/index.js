import React, { useState } from 'react';
import { Marker, InfoWindow } from 'react-google-maps';
import { STATUS } from '../../../helpers/constants';

const MarkerUI = ({ marker }) => {
  const currentStyle = STATUS.find(status => status.data === marker.status);
  const [isShowInfo, setShowInfo] = useState(true);
  return (
    <Marker
      icon={{
        url: `http://maps.google.com/mapfiles/ms/icons/${currentStyle.color}.png`
      }}
      onClick={() => setShowInfo(!isShowInfo)}
      position={{ lat: Number(marker.lat), lng: Number(marker.lng) }}
    >
      {isShowInfo && currentStyle && (
        <InfoWindow onCloseClick={() => setShowInfo(!isShowInfo)}>
          <div style={{ fontWeight: 'bold' }}>
            <p>{marker.giverName}</p>
            <p>{marker.address}</p>
          </div>
        </InfoWindow>
      )}
    </Marker>
  );
};

export default MarkerUI;
