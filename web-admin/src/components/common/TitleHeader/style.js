import styled from 'styled-components';

const primaryColor = props => props.theme.palette.header.button;
const fontSecondaryBold = props => props.theme.fonts.secondaryBold;

export default styled.h1`
  ${'' /* color: ${primaryColor}; */}
  color: #2c6dab;
  font-size: 2em;
  font-weight: 600;
  font-family: ${fontSecondaryBold};
`;
