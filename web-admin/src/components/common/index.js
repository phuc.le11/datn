import ButtonGoogle from './ButtonGoogle';
import PageTitle from './PageTitle';
import TitleHeader from './TitleHeader';
import PageContent from './PageContent';
import CustomHeader from './CustomHeader';
import CustomSlider from './CustomSlider';
import CustomUpload from './CustomUpload';

export {
  ButtonGoogle,
  PageTitle,
  TitleHeader,
  PageContent,
  CustomHeader,
  CustomSlider,
  CustomUpload
};
