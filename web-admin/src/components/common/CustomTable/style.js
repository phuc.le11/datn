import styled from 'styled-components';

export default styled.div`
  padding: 20px;
  background: #fff;
  border-radius: 7px;

  .ant-table-fixed, .ant-table {
    border: none !important;
    border-radius: 5px;
  }

  .ant-table-bordered .ant-table-thead > tr > th, .ant-table-bordered .ant-table-tbody > tr > td {
    border-right: none !important;
    font-size: 14px;
  }

  .pagingButtons {
    width: fit-content;
    margin: auto;
    padding-top: 24px;
  }
  .email-field {
    margin-left: 10px;
  }
  .ant-table-thead > tr > th {
    color: #808385;
    ${'' /* text-transform: uppercase; */}
    font-size: 14px;
    font-family: 'Barlow-regular', sans-serif;
  }
  .ant-table-tbody > tr > td {
    padding: 20px 14px;
  }

  .ant-table-row {
    background-color: #fff;
    color: #071924;
  }
`;
