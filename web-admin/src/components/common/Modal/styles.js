import styled from 'styled-components';
import { Modal } from 'antd';

export const ModalStyles = styled(Modal)`
  .ant-modal-content {
    height: 70vh;
    /* overflow: hidden; */
    /* display: flex; */
    /* flex-direction: column; */
    overflow: auto;
  }
`;

export default styled.div`
  width: fit-content !important;
  height: fit-content;
  z-index: 1000;
  background-color: white;
  box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.75);
  .modalHeader {
    align-items: center;
    color: white;
    background-color: #5c5f61;
    display: flex;
    header {
      font-weight: bold;
      flex-grow: 4;
      margin: 10px 30px;
    }
    .ant-btn {
      background-color: inherit;
      border: none;
      padding: 0;
      color: inherit;
      margin-right: 30px;
    }
    div {
      .ant-btn {
        padding: 0;
        margin-right: 24px;
      }
    }
  }
  .modalBody {
    width: 540px;
    overflow-y: scroll;
    max-height: 600px;
    .ant-form {
      margin: 30px;
      margin-top: 20px;
      input {
        color: #071924 !important;
      }
      .ant-form-item-label {
        line-height: 33px !important;
        label {
          color: #99a2a8;
        }
      }
      .ant-form-item-required::before {
        display: none;
      }
      textarea.ant-input {
        height: 40px;
        min-height: 40px !important;
        background-color: #eef0f1;
      }
    }
    .ant-form .ant-row {
      margin-bottom: 10px;
      &:last-child {
        margin: 0;
      }
    }
    .ant-form .ant-form-item {
      &:last-child {
        margin-top: 36px;
        display: flex;
        justify-content: flex-end;
      }
      .ant-form-item-children {
        input,
        .ant-select-selection {
          height: 40px;
          border-radius: 2px;
          background-color: #eef0f1;
        }
        .ant-btn {
          width: 100px;
          height: 40px;
          border-radius: 2px;
          margin-left: 16px;
        }
        .ant-upload .ant-btn {
          margin: 0px;
          background-color: #eef0f1;
        }
      }
      .ant-upload-list-item {
        display: none;
      }
      .ant-upload-list-item:last-child {
        display: block;
      }
    }
  }
  .modalBody .ant-form .ant-col .ant-form-item {
    margin-top: 0px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  }
`;
