import React from 'react';
import PropTypes from 'prop-types';
import PageTitleWrapper from './styles';
import { Typography } from 'antd';

const PageTitle = ({ children, extraAction }) => (
  <PageTitleWrapper>
    <Typography.Title level={3}>{children}</Typography.Title>
    <div className="extraAction">{extraAction}</div>
  </PageTitleWrapper>
);

PageTitle.propTypes = {
  children: PropTypes.any,
  extraAction: PropTypes.any,
};

export default PageTitle;
