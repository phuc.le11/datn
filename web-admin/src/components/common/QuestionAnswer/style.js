import styled from 'styled-components';

export default styled.li`
  .question {
    color: ${({ theme }) => theme.palette.secondary[1]};
    font-weight: bold;
    &::before {
      counter-increment: section;
      content: counter(section) '. ';
    }
  }
  .answer {
    font-size: 1em;
    color: ${({ theme }) => theme.palette.primary};
    padding-left: 18px;
  }
`;
