import styled from 'styled-components';

const colorButton = props => props.theme.palette.header.button;

export default styled.div`
  .page-header {
    margin-bottom: 34px;
    width: 100%;
    display: flex;
    align-items: start;
    justify-content: flex-end;
    .ant-btn {
      width: 125px;
      height: 30px;
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
      box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
      font-size: 0.875em;
      border-radius: 2px;
      background-color: #fff;
    }
    .ant-btn-primary {
      color: #fff;
      background: #ff6952;
      border-radius: 6px;
      border: none;
      ${'' /* background-color: ${colorButton}; */}
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
      box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
      font-size: 20px;
      padding: 6px 0;
      height: auto;
    }
  }
`;
