import React from 'react';
import { Menu, Icon, Layout } from 'antd';
import { findLast } from 'lodash';
import { history } from '../../../redux/store';
import StyledSlider from './style';
import { PRIVATE_ROUTES } from '../../../routes/PrivateRoutes';
import logo from '../../../assets/images/logo.png';

const { Sider } = Layout;

const CustomSlider = ({ collapsed, toggle }) => {
  const defaultSelectedKeys =
    findLast(
      PRIVATE_ROUTES,
      menu => window.location.pathname.indexOf(menu.path) === 0
    ) || PRIVATE_ROUTES[0];

  return (
    <StyledSlider className="sidebar-wrapper">
      <aside className="left">
        <Sider
          collapsible
          collapsed={collapsed}
          className={`sidebar ${!collapsed && 'open-sidebar'}`}
          collapsedWidth={64}
          width={240}
          onCollapse={toggle}
        >
          <div className="logo-wrapper">
            <img src={logo} alt="" />
          </div>
          <Menu mode="inline" defaultSelectedKeys={[defaultSelectedKeys.key]}>
            {PRIVATE_ROUTES.map(menu => (
              <Menu.Item key={menu.key} onClick={() => history.push(menu.path)}>
                <Icon component={menu.icon} />
                <span>{menu.text}</span>
              </Menu.Item>
            ))}
          </Menu>
        </Sider>
      </aside>
    </StyledSlider>
  );
};

export default CustomSlider;
