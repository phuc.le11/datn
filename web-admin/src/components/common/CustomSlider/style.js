import styled from 'styled-components';

export default styled.div`
  ${'' /* overflow: hidden; */}
  height: auto;
  left: 0;
  ${'' /* background: #001529; */}
  background: #44379e;
  input#collapsedTracker {
    display: none;
  }
  
  .sidebar {
    overflow: hidden;
    ${'' /* top: 64px; */}
    top: 20px;
    left: 0;
    ${'' /* background: #001529; */}
    background: #44379e;
    box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 6px 3px;
    ${'' /* position: fixed; */}
    z-index: 1000;
    height: calc(100vh - 64px);

    .logo-wrapper {
      width: 50px;
      height: 50px;
      border-radius: 50%;
      overflow: hidden;
      padding: 10px;
      display: flex;
      align-items: center;
      justify-content: center;
      background: #ff6952;
      margin: 50px auto;

      img {
        width: 100%;
        object-fit: contain;
      }
    }

    .ant-layout-sider-children {
      display: flex;
      flex-direction: column;
    }
    .ant-menu {
      font-size: 1em;
      width: 240px;
      box-sizing: border-box;
      ${'' /* background: #001529; */}
      background: #44379e;
      padding: 16px 4px;
    }
    .ant-menu-inline, .ant-menu-inline-collapsed {
      border-right: 0;
      .ant-menu-item {
        margin-top: 0;
        color: #ffffffa6;

        &:hover {
          color: #fff;
        }
      }
    }
    .ant-menu-inline-collapsed {
      width: 64px;
    }
  }
  .left {
    ${'' /* background: #001529; */}
    background: #44379e;
  }
  .open-sidebar {
    .ant-menu {
      padding: 16px !important;
    }
  }

  .ant-layout-sider-trigger {
    background: #44379e;
    border-top: 1px solid #6c5dd3;
  }

  .ant-layout-sider-children {
    display: flex;
    flex-direction: column;
  }
  .ant-menu-inline-collapsed {
    width: 64px;
  }
  .trigger {
    width: 64px;
    font-size: 20px;
    line-height: 64px;
    cursor: pointer;
    transition: color 0.3s;

    &:hover {
      color: ${({ theme }) => theme.palette.primary};
    }
    @media only screen and (max-width: 430px) {
      color: ${({ theme }) => theme.palette.primary};
    }
  }
  #collapsedTracker {
    width: 0px;
    height: 0px;
    position: absolute;
  }
  @media only screen and (max-width: 430px) {
    .sidebar {
      left: -80px;
      ${'' /* position: fixed; */}
      z-index: 9999;
    }
    .overlay {
      z-index: 9998;
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      opacity: 0;
      pointer-events: none;
      background: rgba(0, 0, 0, 0.5);
      transition: all 0.5s ease 0s;
    }
    #collapsedTracker:checked ~ .sidebar {
      left: 0px;
    }
    #collapsedTracker:checked ~ .overlay {
      opacity: 1;
      pointer-events: auto;
    }
  }
  .ant-menu-inline-collapsed .ant-menu-item {
    font-size: 1em;
    padding: 0px 0px 0px 22px !important;
    height: 60px;
    margin: 0;
    justify-content: flex-start;
    display: flex;
    align-items: center;
    border: 0;
    .anticon {
      font-size: 16px;
    }
  }
  .ant-menu .ant-menu-item {
    font-size: 1em;
    height: 60px;
    display: flex;
    align-items: center;
    .anticon {
      font-size: 1em;
    }
  }
  .ant-menu-vertical .ant-menu-item:not(:last-child),
  .ant-menu-vertical-left .ant-menu-item:not(:last-child),
  .ant-menu-vertical-right .ant-menu-item:not(:last-child),
  .ant-menu-inline .ant-menu-item:not(:last-child) {
    margin-bottom: 0px;
  }
  .ant-menu:not(.ant-menu-horizontal) .ant-menu-item-selected {
    ${'' /* background-color: ${({ theme }) => theme.palette.primary}; */}
    ${'' /* background-color: #1890ff; */}
    background-color: #6c5dd3;
    overflow: hidden;
    border-radius: 10px;
    color: white;
    svg {
      path {
        fill: white;
      }
    }
  }
  .ant-menu-vertical {
    border-right: 0;
  }
  .ant-menu-vertical .ant-menu-item::after,
  .ant-menu-vertical-left .ant-menu-item::after,
  .ant-menu-vertical-right .ant-menu-item::after,
  .ant-menu-inline .ant-menu-item::after {
    border-right: 0;
  }
`;
