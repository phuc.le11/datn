import React from 'react';
import PropTypes from 'prop-types';
import { Form, DatePicker } from 'antd';

const CustomDatePicker = ({ form, label, src, colon, required, message }) => {
  const { getFieldDecorator } = form;
  return (
    <Form.Item label={label} colon={colon}>
      {getFieldDecorator(src, {
        rules: [
          {
            required,
            message
          }
        ]
      })(<DatePicker />)}
    </Form.Item>
  );
};

CustomDatePicker.propTypes = {
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func.isRequired
  }),
  label: PropTypes.string,
  src: PropTypes.string.isRequired,
  colon: PropTypes.bool,
  required: PropTypes.bool,
  message: PropTypes.string
};

CustomDatePicker.defaultProps = {
  colon: true,
  required: false,
  message: null
};

export default CustomDatePicker;
