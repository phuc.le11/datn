import styled from 'styled-components';
import { Layout } from 'antd';

const { Header } = Layout;

export default styled(Header)`
  position: 'fixed';
  z-index: 1;
  background: #f8f9fd;
  padding: 0 30px;
  display: grid;
  grid-template-columns: auto auto auto;
  grid-template-areas: '....... leftHeader rightHeader';
  transition: all 0.5s ease 0.2s;
  box-shadow: none !important;
  height: 112px;
  padding: 24px 40px;

  .leftHeader {
    display: flex;
    align-items: center;

    h3 {
      margin: 0;
      font-size: 26px;
      font-weight: 800;
      font-family: 'Barlow-SemiBold';
    }
  }

  @media only screen and (max-width: 430px) {
    display: inherit;
  }
  h1 {
    grid-area: leftHeader;
    justify-content: center;
    display: flex;
    margin-left: 105px;
    @media only screen and (max-width: 430px) {
      width: 100%;
      display: inherit;
      padding-right: 45px;
    }
  }
  .rightHeader {
    grid-area: rightHeader;
    display: flex;
    height: 64px;
    flex-direction: row-reverse;
    align-items: center;
    line-height: 24px;
    text-align: center;
    .info {
      margin-right: 10px;
      .name {
        font-size: 14px;
        line-height: 1.57;
        margin: 0;
        color: #071924;
        font-weight: 500;
        font-style: normal;
        font-stretch: normal;
        letter-spacing: normal;
        text-align: right;
        font-family: 'Barlow-medium', sans-serif;
      }
      .role {
        font-size: 12px;
        color: ##808385;
        margin: 0;
        font-family: 'Barlow-regular', sans-serif;
      }
    }
    @media only screen and (max-width: 430px) {
      display: none;
    }
  }
  .title {
    display: none;
    opacity: 0;
    transition: opacity 0.3s;
    text-align: center;
    @media only screen and (max-width: 430px) {
      opacity: 1;
      display: inline-block;
      padding-left: 20px;
      font-size: 20px;
      font-weight: 500;
      width: 100%;
    }
  }

  & .ant-avatar-lg.ant-avatar-icon {
    .anticon {
      font-size: 24px;
    }
  }
`;
