import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Menu, Dropdown, Avatar, Icon } from 'antd';
import { connect } from 'react-redux';
import { TitleHeader } from '..';
import {
  logoutFirebaseAction,
  getCurrentUserFirebaseAction,
} from '../../../redux/auth/actions';
import PageTitle from '../../../components/common/PageTitle';
import StyledHeader from './style';
import { HamburgerIcon } from '../../../assets/icons';

const CustomHeader = ({
  logoutFirebase,
  currentUser,
  getCurrentUserFirebase,
  toggle,
}) => {
  const customBreadcrumb = [
    {
      pageTitle: 'Donations',
      path: '/',
    },
    {
      pageTitle: 'Volunteers',
      path: '/volunteers',
    },
    {
      pageTitle: 'Events',
      path: '/events',
    },
  ]
  const profileMenu = [
    // {
    //   key: 'profile',
    //   text: 'Profile',
    //   url: '#',
    // },
  ];
  useEffect(() => {
    getCurrentUserFirebase();
  }, [getCurrentUserFirebase]);
  return (
    <>
      <StyledHeader>
        <div className="leftHeader">
          {/* <Icon className="trigger" component={HamburgerIcon} onClick={toggle} /> */}
          {customBreadcrumb.map((item) =>
          window.location.pathname === item.path ? (
            <PageTitle key={item.pageTitle}>{item.pageTitle}</PageTitle>
          ) : (
              ''
            ),
        )}
        </div>
        {/* <TitleHeader /> */}

        <div className="rightHeader">
          <Dropdown
            overlay={
              <Menu style={{ minWidth: '120px' }}>
                {profileMenu.map(menu => (
                  <Menu.Item key={menu.key}>
                    <a href={menu.url}>{menu.text}</a>
                  </Menu.Item>
                ))}
                <Menu.Divider />
                <Menu.Item
                  onClick={() => {
                    logoutFirebase();
                  }}
                  key="logout"
                >
                  Logout
                </Menu.Item>
              </Menu>
            }
            trigger={['click']}
          >
            <Avatar size="large" src={currentUser.photoURL} />
          </Dropdown>
          <div className="info">
            <p className="name"> {currentUser.displayName}</p>
          </div>
        </div>
      </StyledHeader>
    </>
  );
};

CustomHeader.propTypes = {
  logoutFirebase: PropTypes.func.isRequired,
  currentUser: PropTypes.shape({
    photoURL: PropTypes.string,
    displayName: PropTypes.string,
  }),
  getCurrentUserFirebase: PropTypes.func.isRequired,
};

CustomHeader.defaultProps = {
  currentUser: {},
};

export default memo(
  connect(
    state => ({
      isAuthenticated: state.auth.isAuthenticated,
      currentUser: state.auth.data,
    }),
    dispatch => ({
      logoutFirebase: () => dispatch(logoutFirebaseAction()),
      getCurrentUserFirebase: () => dispatch(getCurrentUserFirebaseAction()),
    }),
  )(CustomHeader),
);
