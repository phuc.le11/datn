import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const DonationsTypes = makeConstantCreator(
  'GET_ALL_DONATIONS',
  'GET_DONATIONS_SUCCESS',
  'GET_DONATIONS_FAIL',
  'GET_NEXT_DONATIONS',
  'GET_PREVIOUS_DONATIONS',
  'CREATE_DONATION',
  'CREATE_DONATION_SUCCESS',
  'CREATE_DONATION_FAIL',
  'DELETE_DONATION',
  'DELETE_DONATION_SUCCESS',
  'DELETE_DONATION_FAIL',
  'EDIT_DONATION',
  'EDIT_DONATION_SUCCESS',
  'EDIT_DONATION_FAIL',
  'UNSUBSCRIBE',
  'UPDATE_STATUS',
  'UPDATE_STATUS_SUCCESS',
  'UPDATE_NEW_STATUS',
  'UPDATE_NEW_STATUS_SUCCESS'
);

export const updateStatusSuccessAction = ({ id }) =>
  makeActionCreator(DonationsTypes.UPDATE_STATUS_SUCCESS, { id });

export const createDonationAction = ({ donation }) =>
  makeActionCreator(DonationsTypes.CREATE_DONATION, {
    donation
  });

export const createDonationSuccessAction = () =>
  makeActionCreator(DonationsTypes.CREATE_DONATION_SUCCESS);

export const createDonationFailAction = () =>
  makeActionCreator(DonationsTypes.CREATE_DONATION_FAIL);

export const deleteDonationAction = ({ id }) =>
  makeActionCreator(DonationsTypes.DELETE_DONATION, { id });

export const updateStatusAction = ({ idVolunteer, idDonation }) =>
  makeActionCreator(DonationsTypes.UPDATE_STATUS, {
    idVolunteer,
    idDonation
  });
export const updateNewStatusSuccessAction = ({id}) => 
  makeActionCreator(DonationsTypes.UPDATE_NEW_STATUS_SUCCESS, {id})

export const updateNewStatusAction = ({status, idDonation}) => 
  makeActionCreator(DonationsTypes.UPDATE_NEW_STATUS, {
    status,
    idDonation
  })

export const deleteDonationSuccessAction = ({ id }) =>
  makeActionCreator(DonationsTypes.DELETE_DONATION_SUCCESS, { id });

export const deleteDonationFailAction = ({ error }) =>
  makeActionCreator(DonationsTypes.DELETE_DONATION_FAIL, { error });

export const editDonationAction = ({ id, donation }) =>
  makeActionCreator(DonationsTypes.EDIT_DONATION, {
    id,
    donation
  });

export const editDonationSuccessAction = ({ id, donation }) =>
  makeActionCreator(DonationsTypes.EDIT_DONATION_SUCCESS, {
    id,
    donation
  });

export const editDonationFailAction = ({ error }) =>
  makeActionCreator(DonationsTypes.EDIT_DONATION_FAIL, { error });

export const getAllDonationsAction = () =>
  makeActionCreator(DonationsTypes.GET_ALL_DONATIONS);

export const getDonationsSuccessAction = ({ donations }) =>
  makeActionCreator(DonationsTypes.GET_DONATIONS_SUCCESS, {
    donations
  });

export const getDonationsFailAction = ({ error }) =>
  makeActionCreator(DonationsTypes.GET_DONATIONS_FAIL, { error });

export const unSubscribeAction = () =>
  makeActionCreator(DonationsTypes.UNSUBSCRIBE);
