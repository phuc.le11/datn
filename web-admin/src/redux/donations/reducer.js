import { makeReducerCreator } from '../../utils/reduxUtils';
import { DonationsTypes } from './actions';

export const initialState = {
  donations: [],
  error: null
};

const createDonationSuccess = state => ({
  ...state,
  error: null
});

const updateStatusSuccess = (state, action) => ({
  ...state,
  donations: state.donations.map(donation =>
    donation.id === action.id
      ? { ...donation, status: 'pending' }
      : donation
  )
});

const createDonationFail = (state, action) => ({
  ...state,
  error: action.error
});

const deleteDonationSuccess = (state, action) => ({
  ...state,
  error: null,
  DONATIONsByPage: state.DONATIONsByPage.filter(
    DONATION => DONATION.id !== action.id
  )
});

const deleteDonationFail = (state, action) => ({
  ...state,
  error: action.error
});

const editDonationSuccess = (state, action) => ({
  ...state,
  error: null,
  DONATIONsByPage: state.DONATIONsByPage.map(DONATION =>
    DONATION.id === action.id
      ? { ...DONATION, role: action.DONATION.role }
      : DONATION
  )
});

const editDonationFail = (state, action) => ({
  ...state,
  error: action.error
});

const getDonationsSuccess = (state, action) => ({
  ...state,
  donations: action.donations
});

const getDonationFail = (state, action) => ({
  ...state,
  error: action.error
});

const donations = makeReducerCreator(initialState, {
  [DonationsTypes.CREATE_DONATION_FAIL]: createDonationFail,
  [DonationsTypes.CREATE_DONATION_SUCCESS]: createDonationSuccess,
  [DonationsTypes.DELETE_DONATION_FAIL]: deleteDonationFail,
  [DonationsTypes.DELETE_DONATION_SUCCESS]: deleteDonationSuccess,
  [DonationsTypes.EDIT_DONATION_FAIL]: editDonationFail,
  [DonationsTypes.EDIT_DONATION_SUCCESS]: editDonationSuccess,
  [DonationsTypes.GET_DONATIONS_FAIL]: getDonationFail,
  [DonationsTypes.GET_DONATIONS_SUCCESS]: getDonationsSuccess,
  [DonationsTypes.UPDATE_STATUS_SUCCESS]: updateStatusSuccess
});

export default donations;
