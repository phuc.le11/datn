import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import { notification } from 'antd';
import firebase from 'firebase';
import {
  DonationsTypes,
  createDonationFailAction,
  deleteDonationFailAction,
  editDonationFailAction,
  getDonationsFailAction,
  getDonationsSuccessAction,
  deleteDonationSuccessAction,
  updateStatusSuccessAction,
  // updateNewStatusSuccessAction
} from './actions';

function* updateStatusSaga({ idVolunteer, idDonation }) {
  try {
    yield firebase
      .firestore()
      .collection('donations')
      .doc(idDonation)
      .update({
        status: 'processing',
        volunteerId: idVolunteer,
      });
    yield put(updateStatusSuccessAction({ idDonation }));
  } catch (e) {
    console.error(e);
  }
}

function* updateNewStatusSaga({ status, idDonation }) {
  try {
    yield firebase
      .firestore()
      .collection('donations')
      .doc(idDonation)
      .update({
        status,
      });
  } catch (e) {
    console.error(e);
  }
}

function* createDonationSaga({ donation }) {
  try {
    yield firebase
      .firestore()
      .collection('donations')
      .add(donation);
  } catch (e) {
    yield put(createDonationFailAction(e));
  }
}

function* editDonationSaga({ id, donation }) {
  try {
    yield firebase
      .firestore()
      .collection('donations')
      .doc(id)
      .update(donation);
  } catch (e) {
    yield put(editDonationFailAction(e));
  }
}

function* deleteDonationSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('donations')
      .doc(id)
      .delete();
    yield put(deleteDonationSuccessAction({ id }));
  } catch (e) {
    yield put(deleteDonationFailAction(e));
  }
}

let channel = null;

function* getAllDonationsSaga() {
  try {
    let getAll = true;
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('donations')
        .orderBy('createdDate', 'desc')
        .onSnapshot(docs => {
          docs.docChanges().forEach(change => {
            change.type === 'modified' &&
              change.doc.data().status === 'processing' &&
              notification.success({
                message: 'Volunteer assigned',
                description: `${
                  change.doc.data().giverName
                }'s case has been taken`,
              });
            !getAll && change.type === 'added' &&
              notification.success({
                message: 'Have a new one',
                description: `${
                  change.doc.data().giverName
                }'s case has been created`,
              });
          });
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      getAll = false;
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      });
      yield put(getDonationsSuccessAction({ donations: responseData }));
    }
  } catch (e) {
    yield put(getDonationsFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

export default [
  takeEvery(DonationsTypes.CREATE_DONATION, createDonationSaga),
  takeEvery(DonationsTypes.DELETE_DONATION, deleteDonationSaga),
  takeEvery(DonationsTypes.EDIT_DONATION, editDonationSaga),
  takeEvery(DonationsTypes.GET_ALL_DONATIONS, getAllDonationsSaga),
  takeEvery(DonationsTypes.UNSUBSCRIBE, unSubscribeSaga),
  takeEvery(DonationsTypes.UPDATE_STATUS, updateStatusSaga),
  takeEvery(DonationsTypes.UPDATE_NEW_STATUS, updateNewStatusSaga),
];
