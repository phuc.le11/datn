import { all } from 'redux-saga/effects';
import authSaga from './auth/sagas';
import firebaseImg from './firebaseImg/sagas';
import donationsSaga from './donations/sagas';
import volunteersSaga from './volunteers/sagas';
import eventsSaga from './events/sagas';

export default function* root() {
  yield all([
    ...authSaga,
    ...firebaseImg,
    ...donationsSaga,
    ...volunteersSaga,
    ...eventsSaga,
  ]);
}
