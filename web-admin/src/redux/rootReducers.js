import { connectRouter } from 'connected-react-router';
import { auth } from './auth/reducer';
import modal from './modal/reducer';
import loading from './loading/reducer';
import pagingButtons from './pagingButtons/reducer';
import volunteers from './volunteers/reducer';
import donations from './donations/reducer';
import events from './events/reducer';

export default history => ({
  router: connectRouter(history),
  auth,
  loading,
  modal,
  pagingButtons,
  volunteers,
  donations,
  events,
});
