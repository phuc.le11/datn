import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import {
  VolunteersTypes,
  createVolunteerFailAction,
  deleteVolunteerFailAction,
  editVolunteerFailAction,
  getVolunteersFailAction,
  getVolunteersSuccessAction,
  // getAllVolunteersAction,
  deleteVolunteerSuccessAction,
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  // defaultButtonsAction
} from '../pagingButtons/actions';

function* createVolunteerSaga({ volunteer }) {
  try {
    yield firebase
      .firestore()
      .collection('volunteers')
      .add(volunteer);

    yield firebase
      .auth()
      .createUserWithEmailAndPassword(volunteer.email, '123456')
  } catch (e) {
    yield put(createVolunteerFailAction(e));
  }
}

function* editVolunteerSaga({ id, volunteer }) {
  try {
    yield firebase
      .firestore()
      .collection('volunteers')
      .doc(id)
      .update(volunteer);
  } catch (e) {
    yield put(editVolunteerFailAction(e));
  }
}

function* deleteVolunteerSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('volunteers')
      .doc(id)
      .delete();
    yield put(deleteVolunteerSuccessAction({ id }));
  } catch (e) {
    yield put(deleteVolunteerFailAction(e));
  }
}

let channel = null;

function* getAllVolunteersSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('volunteers')
        .orderBy('name')
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      });
      yield put(getVolunteersSuccessAction({ volunteers: responseData }));
    }
  } catch (e) {
    yield put(getVolunteersFailAction(e));
  }
}

function* getNextVolunteersSaga({ lastVolunteer }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('volunteers')
      .orderBy('name')
      .startAfter(lastVolunteer.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('volunteers')
          .orderBy('name')
          .startAfter(lastVolunteer.name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(
          getVolunteersSuccessAction({ volunteersByPage: responseData }),
        );
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (e) {
    yield put(getVolunteersFailAction(e));
  }
}

function* getPreviousVolunteersSaga({ firstVolunteer }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('volunteers')
      .orderBy('name', 'desc')
      .startAfter(firstVolunteer.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('volunteers')
          .orderBy('name')
          .startAt(response.docs[9].data().name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(
          getVolunteersSuccessAction({ volunteersByPage: responseData }),
        );
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (e) {
    yield put(getVolunteersFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

export default [
  takeEvery(VolunteersTypes.CREATE_VOLUNTEER, createVolunteerSaga),
  takeEvery(VolunteersTypes.DELETE_VOLUNTEER, deleteVolunteerSaga),
  takeEvery(VolunteersTypes.EDIT_VOLUNTEER, editVolunteerSaga),
  takeEvery(VolunteersTypes.GET_ALL_VOLUNTEERS, getAllVolunteersSaga),
  takeEvery(VolunteersTypes.GET_NEXT_VOLUNTEERS, getNextVolunteersSaga),
  takeEvery(VolunteersTypes.GET_PREVIOUS_VOLUNTEERS, getPreviousVolunteersSaga),
  takeEvery(VolunteersTypes.UNSUBSCRIBE, unSubscribeSaga),
];
