import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const VolunteersTypes = makeConstantCreator(
  'GET_ALL_VOLUNTEERS',
  'GET_VOLUNTEERS_SUCCESS',
  'GET_VOLUNTEERS_FAIL',
  'GET_NEXT_VOLUNTEERS',
  'GET_PREVIOUS_VOLUNTEERS',
  'CREATE_VOLUNTEER',
  'CREATE_VOLUNTEER_SUCCESS',
  'CREATE_VOLUNTEER_FAIL',
  'DELETE_VOLUNTEER',
  'DELETE_VOLUNTEER_SUCCESS',
  'DELETE_VOLUNTEER_FAIL',
  'EDIT_VOLUNTEER',
  'EDIT_VOLUNTEER_SUCCESS',
  'EDIT_VOLUNTEER_FAIL',
  'UNSUBSCRIBE'
);

export const createVolunteerAction = ({ volunteer }) =>
  makeActionCreator(VolunteersTypes.CREATE_VOLUNTEER, { volunteer });

export const createVolunteerSuccessAction = () =>
  makeActionCreator(VolunteersTypes.GET_VOLUNTEERS_SUCCESS);

export const createVolunteerFailAction = ({ error }) =>
  makeActionCreator(VolunteersTypes.GET_VOLUNTEERS_FAIL, { error });

export const deleteVolunteerAction = ({ id }) =>
  makeActionCreator(VolunteersTypes.DELETE_VOLUNTEER, { id });

export const deleteVolunteerSuccessAction = ({ id }) =>
  makeActionCreator(VolunteersTypes.CREATE_VOLUNTEER_SUCCESS, { id });

export const deleteVolunteerFailAction = ({ error }) =>
  makeActionCreator(VolunteersTypes.DELETE_VOLUNTEER_FAIL, { error });

export const editVolunteerAction = ({ id, volunteer }) =>
  makeActionCreator(VolunteersTypes.EDIT_VOLUNTEER, { id, volunteer });

export const editVolunteerSuccessAction = ({ id, volunteer }) =>
  makeActionCreator(VolunteersTypes.EDIT_VOLUNTEER_SUCCESS, { id, volunteer });

export const editVolunteerFailAction = ({ error }) =>
  makeActionCreator(VolunteersTypes.EDIT_VOLUNTEER_FAIL, { error });

export const getAllVolunteersAction = () =>
  makeActionCreator(VolunteersTypes.GET_ALL_VOLUNTEERS);

export const getVolunteersSuccessAction = ({ volunteers }) =>
  makeActionCreator(VolunteersTypes.GET_VOLUNTEERS_SUCCESS, {
    volunteers
  });

export const getVolunteersFailAction = ({ error }) =>
  makeActionCreator(VolunteersTypes.GET_VOLUNTEERS_FAIL, { error });

export const unSubscribeAction = () =>
  makeActionCreator(VolunteersTypes.UNSUBSCRIBE);
