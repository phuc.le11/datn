import { makeReducerCreator } from '../../utils/reduxUtils';
import { PagingButtonsTypes } from './actions';

export const initialState = {
  disableNext: false,
  disablePrev: false,
};

const disableNextButton = state => ({
  ...state,
  disableNext: true,
});

const disablePrevButton = state => ({
  ...state,
  disablePrev: true,
});

const defaultButtons = state => ({
  ...state,
  disableNext: false,
  disablePrev: true,
});

const enableNextButton = state => ({
  ...state,
  disableNext: false,
});

const enablePrevButton = state => ({
  ...state,
  disablePrev: false,
})

const pagingButtons = makeReducerCreator(initialState, {
  [PagingButtonsTypes.DISABLE_NEXT_BUTTON]: disableNextButton,
  [PagingButtonsTypes.DISABLE_PREV_BUTTON]: disablePrevButton,
  [PagingButtonsTypes.DEFAULT_BUTTONS]: defaultButtons,
  [PagingButtonsTypes.ENABLE_NEXT_BUTTON]: enableNextButton,
  [PagingButtonsTypes.ENABLE_PREV_BUTTON]: enablePrevButton,
});

export default pagingButtons;
