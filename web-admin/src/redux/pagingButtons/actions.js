import { makeActionCreator, makeConstantCreator } from '../../utils/reduxUtils';

export const PagingButtonsTypes = makeConstantCreator(
  'DISABLE_NEXT_BUTTON',
  'DISABLE_PREV_BUTTON',
  'ENABLE_NEXT_BUTTON',
  'ENABLE_PREV_BUTTON',
  'DEFAULT_BUTTONS',
);

export const disableNextButtonAction = () =>
  makeActionCreator(PagingButtonsTypes.DISABLE_NEXT_BUTTON);

export const disablePrevButtonAction = () =>
  makeActionCreator(PagingButtonsTypes.DISABLE_PREV_BUTTON);

export const defaultButtonsAction = () =>
  makeActionCreator(PagingButtonsTypes.DEFAULT_BUTTONS);

export const enableNextButtonAction = () =>
  makeActionCreator(PagingButtonsTypes.ENABLE_NEXT_BUTTON);

export const enablePrevButtonAction = () =>
  makeActionCreator(PagingButtonsTypes.ENABLE_PREV_BUTTON);
