import { makeReducerCreator } from '../../utils/reduxUtils';
import { EventsTypes } from './actions';

export const initialState = {
  events: [],
  error: null,
};

const createEventSuccess = state => ({
  ...state,
  error: null,
});

const createEventFail = (state, action) => ({
  ...state,
  error: action.error,
});

const deleteEventSuccess = (state, action) => ({
  ...state,
  error: null,
  eventsByPage: state.eventsByPage.filter(event => event.id !== action.id),
});

const deleteEventFail = (state, action) => ({
  ...state,
  error: action.error,
});

const editEventSuccess = (state, action) => ({
  ...state,
  error: null,
  eventsByPage: state.eventsByPage.map(event =>
    event.id === action.id ? { ...event, role: action.event.role } : event,
  ),
});

const editEventFail = (state, action) => ({
  ...state,
  error: action.error,
});

const getEventsSuccess = (state, action) => ({
  ...state,
  events: action.events,
});

const getEventsFail = (state, action) => ({
  ...state,
  error: action.error,
});

const events = makeReducerCreator(initialState, {
  [EventsTypes.CREATE_EVENT_FAIL]: createEventFail,
  [EventsTypes.CREATE_EVENT_SUCCESS]: createEventSuccess,
  [EventsTypes.DELETE_EVENT_FAIL]: deleteEventFail,
  [EventsTypes.DELETE_EVENT_SUCCESS]: deleteEventSuccess,
  [EventsTypes.EDIT_EVENT_FAIL]: editEventFail,
  [EventsTypes.EDIT_EVENT_SUCCESS]: editEventSuccess,
  [EventsTypes.GET_EVENTS_FAIL]: getEventsFail,
  [EventsTypes.GET_EVENTS_SUCCESS]: getEventsSuccess,
});

export default events;
