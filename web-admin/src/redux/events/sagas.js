import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import {
  EventsTypes,
  createEventFailAction,
  deleteEventFailAction,
  editEventFailAction,
  getEventsFailAction,
  getEventsSuccessAction,
  // getAllVolunteersAction,
  deleteEventSuccessAction,
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  // defaultButtonsAction
} from '../pagingButtons/actions';

function* createEventSaga({ event }) {
  try {
    yield firebase
      .firestore()
      .collection('events')
      .add(event);
  } catch (e) {
    yield put(createEventFailAction(e));
  }
}

function* editEventSaga({ id, event }) {
  try {
    yield firebase
      .firestore()
      .collection('events')
      .doc(id)
      .update(event);
  } catch (e) {
    yield put(editEventFailAction(e));
  }
}

function* deleteEventSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('events')
      .doc(id)
      .delete();
    yield put(deleteEventSuccessAction({ id }));
  } catch (e) {
    yield put(deleteEventFailAction(e));
  }
}

let channel = null;

function* getAllEventsSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('events')
        .orderBy('name')
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      });
      yield put(getEventsSuccessAction({ events: responseData }));
    }
  } catch (e) {
    yield put(getEventsFailAction(e));
  }
}

function* getNextEventsSaga({ lastEvent }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('events')
      .orderBy('name')
      .startAfter(lastEvent.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('events')
          .orderBy('name')
          .startAfter(lastEvent.name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getEventsSuccessAction({ eventsByPage: responseData }));
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (e) {
    yield put(getEventsFailAction(e));
  }
}

function* getPreviousEventsSaga({ firstEvent }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('events')
      .orderBy('name', 'desc')
      .startAfter(firstEvent.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('events')
          .orderBy('name')
          .startAt(response.docs[9].data().name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getEventsSuccessAction({ eventsByPage: responseData }));
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (e) {
    yield put(getEventsFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

export default [
  takeEvery(EventsTypes.CREATE_EVENT, createEventSaga),
  takeEvery(EventsTypes.DELETE_EVENT, deleteEventSaga),
  takeEvery(EventsTypes.EDIT_EVENT, editEventSaga),
  takeEvery(EventsTypes.GET_ALL_EVENTS, getAllEventsSaga),
  takeEvery(EventsTypes.GET_NEXT_EVENTS, getNextEventsSaga),
  takeEvery(EventsTypes.GET_PREVIOUS_EVENTS, getPreviousEventsSaga),
  takeEvery(EventsTypes.UNSUBSCRIBE, unSubscribeSaga),
];
