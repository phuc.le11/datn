import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const EventsTypes = makeConstantCreator(
  'GET_ALL_EVENTS',
  'GET_EVENTS_SUCCESS',
  'GET_EVENTS_FAIL',
  'GET_NEXT_EVENTS',
  'GET_PREVIOUS_EVENTS',
  'CREATE_EVENT',
  'CREATE_EVENT_SUCCESS',
  'CREATE_EVENT_FAIL',
  'DELETE_EVENT',
  'DELETE_EVENT_SUCCESS',
  'DELETE_EVENT_FAIL',
  'EDIT_EVENT',
  'EDIT_EVENT_SUCCESS',
  'EDIT_EVENT_FAIL',
  'UNSUBSCRIBE',
);

export const createEventAction = ({ event }) =>
  makeActionCreator(EventsTypes.CREATE_EVENT, { event });

export const createEventSuccessAction = () =>
  makeActionCreator(EventsTypes.GET_EVENTS_SUCCESS);

export const createEventFailAction = ({ error }) =>
  makeActionCreator(EventsTypes.GET_EVENTS_FAIL, { error });

export const deleteEventAction = ({ id }) =>
  makeActionCreator(EventsTypes.DELETE_EVENT, { id });

export const deleteEventSuccessAction = ({ id }) =>
  makeActionCreator(EventsTypes.CREATE_EVENT_SUCCESS, { id });

export const deleteEventFailAction = ({ error }) =>
  makeActionCreator(EventsTypes.DELETE_EVENT_FAIL, { error });

export const editEventAction = ({ id, event }) =>
  makeActionCreator(EventsTypes.EDIT_EVENT, { id, event });

export const editEventSuccessAction = ({ id, event }) =>
  makeActionCreator(EventsTypes.EDIT_EVENT_SUCCESS, { id, event });

export const editEventFailAction = ({ error }) =>
  makeActionCreator(EventsTypes.EDIT_EVENT_FAIL, { error });

export const getAllEventsAction = () =>
  makeActionCreator(EventsTypes.GET_ALL_EVENTS);

export const getEventsSuccessAction = ({ events }) =>
  makeActionCreator(EventsTypes.GET_EVENTS_SUCCESS, {
    events,
  });

export const getEventsFailAction = ({ error }) =>
  makeActionCreator(EventsTypes.GET_EVENTS_FAIL, { error });

export const unSubscribeAction = () =>
  makeActionCreator(EventsTypes.UNSUBSCRIBE);
