import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Layout, Icon } from 'antd';
import { Redirect } from 'react-router-dom';
import PrivateLayoutWrapper from './styles';
import { CustomHeader, CustomSlider } from '../../components/common';
import { PUBLIC_ROUTES } from '../../routes/PublicRoutes';

const { Content } = Layout;

const PrivateLayout = ({ isAuthenticated, children, location }) => {
  const [collapsed, setCollapsed] = useState(false);

  const toggle = () => {
    setCollapsed(!collapsed);
  };

  if (!PUBLIC_ROUTES.find(e => location.pathname.search(e.path) > -1)) {
    return !isAuthenticated ? (
      <Redirect to="/login" />
    ) : (
      <PrivateLayoutWrapper>
        <Layout className="windowView">
          {/* <input
            id="collapsedTracker"
            type="checkbox"
            checked={!collapsed}
          />
          <label
            role="presentation"
            htmlFor="collapsedTracker"
            className="overlay"
            onClick={toggle}
          /> */}
          <CustomSlider collapsed={collapsed} toggle={toggle} />
          <Layout className="mainView">
            <CustomHeader toggle={toggle} />
            <Content className="container">
              <div className="content">{children}</div>
            </Content>
          </Layout>
        </Layout>
      </PrivateLayoutWrapper>
    );
  }
  return null;
};

PrivateLayout.propTypes = {
  children: PropTypes.array.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired
};

export default connect(state => ({
  isAuthenticated: state.auth.isAuthenticated,
  isLoading: state.loading.isLoading,
  location: state.router.location
}))(PrivateLayout);
