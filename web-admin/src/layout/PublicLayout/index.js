import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import { connect } from 'react-redux';
import PublicLayoutWrapper from './styles';
import LoadingComponent from '../../components/common/LoadingScreenTransparent';

const { Content } = Layout;

const PublicLayout = ({ children, isLoading }) => (
  <PublicLayoutWrapper>
    {isLoading && <LoadingComponent />}
    <Layout className="layout">
      <Content className="main-img" />
      <div className="main-content">{children}</div>
    </Layout>
  </PublicLayoutWrapper>
);

PublicLayout.propTypes = {
  children: PropTypes.any,
  isLoading: PropTypes.bool.isRequired,
};

export default connect(state => ({
  isLoading: state.loading.isLoading,
}))(PublicLayout);
