import React, { useEffect, useState } from 'react';
import { connect, useSelector } from 'react-redux';
import i18n from 'i18next';
import styled from 'styled-components';
import moment from 'moment';
import { Icon, Dropdown, Menu, Table, Button, List, Drawer } from 'antd';
import {
  deleteVolunteerAction,
  getAllVolunteersAction,
  unSubscribeAction,
} from '../../redux/volunteers/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';
import TableWrapper from '../../components/common/CustomTable/style';
import { getAllDonationsAction } from '../../redux/donations/actions';

const DrawerWrapper = styled(Drawer)`
  .ant-drawer-header {
    background: #1aa768;
    .ant-drawer-title {
      color: #ffff;
    }
  }
`;

const DrawerWrapperProcess = styled(Drawer)`
  .ant-drawer-header {
    background: #1890ff;
    .ant-drawer-title {
      color: #ffff;
    }
  }
`;

const VolunteerContainer = ({
  showModalWithItem,
  deleteVolunteer,
  getAllVolunteers,
  getPreviousVolunteers,
  getNextVolunteers,
  lastVolunteer,
  firstVolunteer,
  disableNext,
  disablePrev,
  unSubscribe,
  getImageUrl,
  getAllDonations,
}) => {
  useEffect(() => {
    getAllVolunteers();
    getAllDonations();
    return () => {
      unSubscribe();
    };
  }, [getAllVolunteers, getAllDonations, unSubscribe]);
  const { volunteers } = useSelector(state => state.volunteers);
  const { donations } = useSelector(state => state.donations);
  const arrDonations = donations.map(item => ({
    giverName: item.giverName,
    description: item.description,
    status: item.status,
    address: item.address,
    phoneNumber: item.phoneNumber,
    volunteerId: item.volunteerId,
  }));

  const listDonationsSuccess = arrDonations.filter(
    e => e.status === 'success',
  );

  const listDonationsProcessing = arrDonations.filter(
    e => e.status === 'processing',
  );

  const [modal, setModal] = useState({});
  const [modalProcessing, setModalProcessing] = useState({});

  const showModalSuccess = id => {
    setModal({
      [id]: true,
    });
  };
  const onCancel = () => {
    setModal({});
  };

  const showModalProcessing = id => {
    setModalProcessing({
      [id]: true,
    });
  };
  const onCancelProcessing = () => {
    setModalProcessing({});
  };

  const gotoEditPage = record => {
    const route = '/volunteers/edit';
    showModalWithItem({ route, item: record });
  };
  const handleDelete = record => {
    deleteVolunteer({ id: record.id });
  };

  const menuItems = [
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDelete,
    },
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage,
    },
  ];
  const columns = [
    {
      title: i18n.t('table.title.name'),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: i18n.t('table.title.phoneNumber'),
      dataIndex: 'phoneNumber',
      key: 'phoneNumber',
    },
    {
      title: i18n.t('table.title.address'),
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: i18n.t('table.title.birthday'),
      dataIndex: 'birthDay',
      render: birthDay => <span>{moment(birthDay).format('DD/MMM/YYYY')}</span>,
      key: 'birthDay',
    },
    {
      title: i18n.t('table.title.email'),
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: i18n.t('table.title.jobProcessing'),
      dataIndex: 'id',
      key: 'id',
      render: row => {
        return (
          <div>
            <Button
              style={{ background: '#1890ff', color: '#ffff' }}
              onClick={() => showModalProcessing(row)}
            >
              LIST PROCESSING
            </Button>
            <DrawerWrapperProcess
              width={480}
              title="List donations Processing"
              placement="right"
              headerStyle={{ color: 'blue' }}
              visible={modalProcessing[row]}
              onClose={onCancelProcessing}
            >
              <List
                itemLayout="horizontal"
                dataSource={listDonationsProcessing.filter(
                  e => e.volunteerId === row,
                )}
                renderItem={item => (
                  <List.Item>
                    <List.Item.Meta
                      title={
                        <>
                          <span style={{ fontWeight: 'bold' }}>
                            Giver Name:{' '}
                          </span>
                          <span>{item && item.giverName}</span>
                        </>
                      }
                      description={
                        <>
                          <p>
                            <Icon
                              type="phone"
                              theme="filled"
                              style={{ marginRight: '5px' }}
                            />
                            {item.phoneNumber}
                          </p>
                          <p>
                            <Icon
                              type="home"
                              theme="filled"
                              style={{ marginRight: '5px' }}
                            />
                            {item.address}
                          </p>
                          <p>
                            <Icon
                              type="container"
                              theme="filled"
                              style={{ marginRight: '5px' }}
                            />
                            {item.description}
                          </p>
                        </>
                      }
                    />
                  </List.Item>
                )}
              />
            </DrawerWrapperProcess>
          </div>
        );
      },
    },
    {
      title: i18n.t('table.title.jobSuccess'),
      dataIndex: 'id',
      key: 'id',
      render: row => {
        return (
          <div>
            <Button type="primary" onClick={() => showModalSuccess(row)}>
              LIST SUCCESS
            </Button>
            <DrawerWrapper
              placement="right"
              width={480}
              title="List donations Success"
              visible={modal[row]}
              onClose={onCancel}
            >
              <List
                itemLayout="horizontal"
                dataSource={listDonationsSuccess.filter(
                  e => e.volunteerId === row,
                )}
                renderItem={item => (
                  <List.Item>
                    <List.Item.Meta
                      title={
                        <>
                          <span style={{ fontWeight: 'bold' }}>
                            Giver Name:{' '}
                          </span>
                          <span>{item && item.giverName}</span>
                        </>
                      }
                      description={
                        <>
                          <p>
                            <Icon
                              type="phone"
                              theme="filled"
                              style={{ marginRight: '5px' }}
                            />
                            {item.phoneNumber}
                          </p>
                          <p>
                            <Icon
                              type="home"
                              theme="filled"
                              style={{ marginRight: '5px' }}
                            />
                            {item.address}
                          </p>
                          <p>
                            <Icon
                              type="container"
                              theme="filled"
                              style={{ marginRight: '5px' }}
                            />
                            {item.description}
                          </p>
                        </>
                      }
                    />
                  </List.Item>
                )}
              />
            </DrawerWrapper>
          </div>
        );
      },
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      fixed: 'right',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={
            <Menu>
              {menuItems.map(menuItem => (
                <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Icon type="more" />
        </Dropdown>
      ),
    },
  ];
  return (
    <>
      <TableWrapper>
        <Table
          bordered
          columns={columns}
          pagination={{ pageSize: 4 }}
          dataSource={volunteers}
          scroll={{ x: 1300 }}
        />
      </TableWrapper>
    </>
  );
};

const mapDispatchToProps = dispatch => ({
  deleteVolunteer: params => dispatch(deleteVolunteerAction(params)),
  showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  getAllDonations: () => dispatch(getAllDonationsAction()),
  getAllVolunteers: () => dispatch(getAllVolunteersAction()),
  unSubscribe: () => dispatch(unSubscribeAction()),
});

export default connect(null, mapDispatchToProps)(VolunteerContainer);
