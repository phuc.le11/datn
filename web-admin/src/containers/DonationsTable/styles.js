import styled from 'styled-components';
import { Button } from 'antd';

export const ButtonWrapper = styled(Button)`
  &.ant-btn-primary {
    color: #fff;
    background: #ff6952;
    background-color: #ff6952;
    border-radius: 6px;
    border: none;
    ${'' /* background-color: ${colorButton}; */}
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
    font-size: 20px;
    padding: 6px 0;
    height: auto;
    width: 125px;
  }
`;

export default styled.div`
  .ant-tabs-tab {
    padding: 0 !important;
    padding-bottom: 16px !important;
  }
  .ant-typography {
    margin: 0;
    color: inherit;
    line-height: normal !important;
  }
`;