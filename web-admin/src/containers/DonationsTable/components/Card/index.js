import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';
import CardStyles from './styles';

const Card = ({ title, value, color, type, backgroundColor }) => {
  return (
    <CardStyles style={{ backgroundColor }}>
      <div className="right-content">
        <Icon style={{ fontSize: '18px', fill: color, color }} type={type} />
      </div>
      <div className="left-content">
        <div className="title">{title}</div>
        <div className="value">{value}</div>
      </div>
    </CardStyles>
  )
}

Card.propTypes = {
  title: PropTypes.string,
  value: PropTypes.number,
  color: PropTypes.string,
  type: PropTypes.string,
}

export default Card;
