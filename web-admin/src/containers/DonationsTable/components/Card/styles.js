import styled from 'styled-components';

export default styled.div`
  background: #fff;
  padding: 14px;
  display: flex;
  justify-content: space-between;
  border-radius: 16px;
  .right-content {
    padding:  0 10px;
    .anticon {
      font-size: 24px !important;
    }
  }

  .left-content {
    width: 70%;
    .title {
      font-size: 18px;
      line-height: normal;
    }

    .value {
      font-size: 32px;
      font-weight: 600;
    }
  }
`;