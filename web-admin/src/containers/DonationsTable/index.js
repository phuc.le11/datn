/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect, useState } from 'react';
import { connect, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import moment from 'moment';
import {
  Icon,
  Tabs,
  Table,
  Modal,
  Select,
  Col,
  Row,
  Typography,
  Drawer,
} from 'antd';
import { withGoogleMap, withScriptjs, GoogleMap } from 'react-google-maps';
import styled from 'styled-components';
import Card from './components/Card';
import MarkerUI from '../../components/Map/Marker';
import {
  deleteDonationAction,
  getAllDonationsAction,
  unSubscribeAction,
  updateStatusAction,
  updateNewStatusAction,
} from '../../redux/donations/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';
import TableWrapper from '../../components/common/CustomTable/style';
import { getAllVolunteersAction } from '../../redux/volunteers/actions';
import { getAllEventsAction } from '../../redux/events/actions';
import DonationStyles, { ButtonWrapper } from './styles';

const { TabPane } = Tabs;
const { Option } = Select;
const { confirm } = Modal;
const { Title } = Typography;

// const { Search } = Input;

// const openNotification = () => {
//   notification.open({
//     message: 'Notification Title',
//     description:
//       'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
//   });
// };

const StyledSelect = styled(Select)`
  .ant-select-selection {
    background: ${props =>
      props.value === 'pending'
        ? '#fff1f0'
        : props.value === 'processing'
        ? '#e6f7ff'
        : '#00800045'};
    color: ${props =>
      props.value === 'pending'
        ? '#f5222d'
        : props.value === 'processing'
        ? '#1890ff'
        : '#fff'};
  }
`;

const DonationContainer = ({
  getAllVolunteers,
  showModalWithItem,
  deleteDonation,
  getAllDonations,
  unSubscribe,
  getImageUrl,
  updateStatus,
  updateNewStatus,
  getAllEvents,
}) => {
  useEffect(() => {
    getAllDonations();
    getAllVolunteers();
    getAllEvents();
    return () => {
      unSubscribe();
    };
  }, [getAllDonations, getAllVolunteers, getAllEvents, unSubscribe]);
  const [isShowMap, setIsShowMap] = useState(false);
  const { donations } = useSelector(state => state.donations);
  const { volunteers } = useSelector(state => state.volunteers);
  const { events } = useSelector(state => state.events);
  const onSelectItem = (idVolunteer, idDonation) => {
    updateStatus({ idVolunteer, idDonation });
  };

  const onSelectStatus = (status, idDonation) => {
    updateNewStatus({ status, idDonation });
  };

  // const gotoEditPage = record => {
  //   const route = '/donations/edit';
  //   showModalWithItem({ route, item: record });
  // };
  const handleDelete = record => {
    confirm({
      title: `Delete information donation`,
      content: 'Are you sure delete this record?',
      okText: 'Ok',
      cancelText: 'Cancel',
      onOk: () => deleteDonation({ id: record.id }),
      onCancel: () => {},
    });
  };
  const LocationMap = withScriptjs(
    withGoogleMap(props => (
      <GoogleMap
        defaultCenter={{ lat: 16.077348, lng: 108.229293 }}
        defaultZoom={15}
      >
        {props.markers.map(marker => (
          <MarkerUI key={marker.volunteerId} marker={marker} />
        ))}
      </GoogleMap>
    )),
  );
  // const menuItems = [
  //   {
  //     id: 0,
  //     text: i18n.t('button.delete'),
  //     handler: handleDelete,
  //   },
  //   {
  //     id: 1,
  //     text: i18n.t('button.edit'),
  //     handler: gotoEditPage,
  //   },
  // ];
  const columns = [
    {
      title: i18n.t('table.title.description'),
      dataIndex: 'description',
      key: 'description',
      // sorter: true,
      render: (description, row) => {
        return (
          <span>
            <div> {description} </div>
            {/* <div style={{ display: 'flex', width: '160px', flexWrap: 'wrap' }}>
              {row.imageURL &&
                row.imageURL.slice(0, 3).map(e => (
                  <img
                    style={{
                      width: '50px',
                      height: '50px',
                      marginRight: '3px',
                      marginBottom: '3px',
                    }}
                    src={e}
                  />
                ))}
            </div> */}
          </span>
        );
      },
    },
    {
      title: i18n.t('table.title.giverName'),
      dataIndex: 'giverName',
      key: 'giverName',
      render: (name, row) => (
        <span>
          <span className="email-field">{name}</span>
        </span>
      ),
    },
    {
      title: i18n.t('table.title.address'),
      dataIndex: 'address',
      key: 'address',
      width: 200,
    },
    {
      title: i18n.t('table.title.date'),
      // dataIndex: 'createdDate',
      // render: createdDate => (
      //   <span>{new Date(createdDate * 1000).toLocaleString()}</span>
      // ),
      // key: 'createdDate',
      dataIndex: 'donationTime',
      key: 'donationTime',
      render: donationTime => (
        <span>
          {moment(donationTime).format('MMM Do, YYYY, HH:mm')}
        </span>
      )
    },
    {
      title: i18n.t('table.title.phoneNumber'),
      dataIndex: 'phoneNumber',
      key: 'phoneNumber',
    },
    {
      title: i18n.t('table.title.eventName'),
      dataIndex: 'eventId',
      key: 'eventId',
      render: eventId => (
        <span>
          {events &&
            events.find(item => item.stt === eventId) &&
            events.find(item => item.stt === eventId).name}
        </span>
      ),
    },

    {
      title: i18n.t('table.title.volunteer'),
      dataIndex: 'volunteerId',
      key: 'volunteerId',
      render: (id, row) => {
        return (
          <span>
            <Select
              value={id}
              style={{ width: 120, margin: '0 10px 0 0' }}
              onSelect={id => onSelectItem(id, row.id)}
            >
              {volunteers &&
                volunteers.map(e => <Option value={e.id}>{e.name}</Option>)}
            </Select>
          </span>
        );
      },
    },
    {
      title: i18n.t('table.title.status'),
      dataIndex: 'status',
      key: 'status',
      render: (status, row) => {
        return (
          <span>
            <StyledSelect
              value={status}
              style={{ width: 120, margin: '0 10px 0 0' }}
              onSelect={status => onSelectStatus(status, row.id)}
            >
              <Option
                // style={{ background: '#fff1f0', color: 'red' }}
                value="pending"
              >
                Pending
              </Option>
              <Option
                // style={{ background: '#e6f7ff', color: '#1890ff' }}
                value="processing"
              >
                Processing
              </Option>
              <Option
                // style={{ background: '#00800045', color: '#52c41a' }}
                value="success"
              >
                Success
              </Option>
            </StyledSelect>
            {/* {!status && (
              <Tag style={{ padding: '4px' }} color="red">
                Pending
              </Tag>
            )}
            {status === 'processing' && (
              <Tag style={{ padding: '4px' }} color="blue">
                Processing
              </Tag>
            )}
            {status === 'success' && (
              <Tag style={{ padding: '4px' }} color="green">
                Success
              </Tag>
            )} */}
          </span>
        );
      },
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      fixed: 'right',
      render: record => (
        <Icon type="delete" onClick={() => handleDelete(record)} />
      ),
    },
  ];
  const [filter, setFilter] = useState('all');
  return (
    <>
    <Drawer visible={isShowMap} closable onClose={() => setIsShowMap(false)} title={null} width={900}>
    <Row gutter={10} type="flex" align="middle" style={{ marginBottom: '20px' }}>
            <Col span={2} style={{ width: '60px' }}>
              <span style={{ fontSize: '14px', color: '#1aa768' }}>Status</span>
            </Col>
            <Col span={7}>
              <Select
                onChange={e => setFilter(e)}
                showSearch
                defaultValue={filter}
                style={{ width: 300 }}
                placeholder="Filter Status"
              >
                <Option value="pending">Pending</Option>
                <Option value="processing">Processing</Option>
                <Option value="success">Success</Option>
                <Option value="all">All</Option>
              </Select>
            </Col>
          </Row>
          <LocationMap
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGZOhb6qWmy1PLYJrLmtBho18Vasw0C_U&v=3.exp&libraries=,drawing"
            containerElement={<div style={{ height: `70vh`, width: '100%' }} />}
            mapElement={<div style={{ height: `100%` }} />}
            loadingElement={<div style={{ height: '100%' }} />}
            markers={filter === 'all'
                  ? donations
                  : donations.filter(e => e.status === filter)}
          />
    </Drawer>
    <Row gutter={24} type="flex">
      <Col span={18}>
      <Row gutter={30} style={{ marginBottom: '24px', marginTop: '20px' }}>
            <Col span={8}>
              <Card
                title="Total Pending"
                type="unordered-list"
                value={donations.filter(e => e.status === 'pending').length}
                color="#f66953"
                backgroundColor="#fef4f1"
                // background: '#fff1f0',
              />
            </Col>
            <Col span={8}>
              <Card
                title="Total Processing"
                color="#4174f5"
                backgroundColor="#e9edfb"
                type="undo"
                value={donations.filter(e => e.status === 'processing').length}
                  // background: '#e6f7ff',
              />
            </Col>
            <Col span={8}>
              <Card
                title="Total Success"
                color="#52c41a"
                value={donations.filter(e => e.status === 'success').length}
                type="check-circle"
                backgroundColor="#e4f5db"
                
                  // background: '#f6ffed',
              />
            </Col>
          </Row>
      </Col>
      <Col span={6}>
        <div style={{
          display: 'flex',
          alignItems: 'flex-end',
          justifyContent: 'flex-end',
          height: '100%',
          padding: '30px 0',
        }}>
          <ButtonWrapper type="primary" onClick={() => setIsShowMap(true)}>View Map</ButtonWrapper>
        </div>
          
      </Col>
    </Row>
          
          <TableWrapper>
          <Row gutter={10} type="flex" align="middle" style={{ marginBottom: '20px' }}>
            <Col span={2} style={{ width: '60px' }}>
              <span style={{ fontSize: '14px', color: '#1aa768' }}>Status</span>
            </Col>
            <Col span={7}>
              <Select
                onChange={e => setFilter(e)}
                showSearch
                defaultValue={filter}
                style={{ width: 300 }}
                placeholder="Filter Status"
              >
                <Option value="pending">Pending</Option>
                <Option value="processing">Processing</Option>
                <Option value="success">Success</Option>
                <Option value="all">All</Option>
              </Select>
            </Col>
          </Row>
            <Table
              scroll={{ x: 1300 }}
              pagination={{ pageSize: 4 }}
              bordered
              columns={columns}
              dataSource={
                filter === 'all'
                  ? donations
                  : donations.filter(e => e.status === filter)
              }
            />
          </TableWrapper>
    </>
  )
  return (
    <DonationStyles>
      <Tabs
        defaultActiveKey="1"
        size="large"
      >
        <TabPane
          tab={<Title level={3}>Donations</Title>}
          key="1"
        >
        <Row gutter={30} style={{ marginBottom: '24px', marginTop: '20px' }}>
            <Col span={8}>
              <Card
                title="Total Pending"
                type="unordered-list"
                value={donations.filter(e => e.status === 'pending').length}
                color="#f5222d"
                // background: '#fff1f0',
              />
            </Col>
            <Col span={8}>
              <Card
                title="Total Processing"
                color="#1890ff"
                type="undo"
                value={donations.filter(e => e.status === 'processing').length}
                  // background: '#e6f7ff',
              />
            </Col>
            <Col span={8}>
              <Card
                title="Total Success"
                color="#52c41a"
                value={donations.filter(e => e.status === 'success').length}
                type="check-circle"
                
                  // background: '#f6ffed',
              />
            </Col>
          </Row>
          
          <TableWrapper>
          <Row gutter={10} type="flex" align="middle" style={{ marginBottom: '20px' }}>
            <Col span={2} style={{ width: '60px' }}>
              <span style={{ fontSize: '14px', color: '#1aa768' }}>Status</span>
            </Col>
            <Col span={7}>
              <Select
                onChange={e => setFilter(e)}
                showSearch
                defaultValue={filter}
                style={{ width: 300 }}
                placeholder="Filter Status"
              >
                <Option value="pending">Pending</Option>
                <Option value="processing">Processing</Option>
                <Option value="success">Success</Option>
                <Option value="all">All</Option>
              </Select>
            </Col>
          </Row>
            <Table
              scroll={{ x: 1300 }}
              pagination={{ pageSize: 4 }}
              bordered
              columns={columns}
              dataSource={
                filter === 'all'
                  ? donations
                  : donations.filter(e => e.status === filter)
              }
            />
          </TableWrapper>
        </TabPane>
        <TabPane
          tab={<Title level={3}>Map</Title>}
          key="2"
        >
        
        </TabPane>
      </Tabs>
    </DonationStyles>
  );
};

DonationContainer.propTypes = {
  employeesByPage: PropTypes.array,
  deleteEmployee: PropTypes.func,
  showModalWithItem: PropTypes.func,
  getAllDonations: PropTypes.func,
  unSubscribe: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  deleteDonation: params => dispatch(deleteDonationAction(params)),
  showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  getAllDonations: () => dispatch(getAllDonationsAction()),
  getAllVolunteers: () => dispatch(getAllVolunteersAction()),
  updateStatus: params => dispatch(updateStatusAction(params)),
  unSubscribe: () => dispatch(unSubscribeAction()),
  updateNewStatus: params => dispatch(updateNewStatusAction(params)),
  getAllEvents: () => dispatch(getAllEventsAction()),
});

export default connect(null, mapDispatchToProps)(DonationContainer);
