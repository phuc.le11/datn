import React, { useEffect, useState } from 'react';
import { connect, useSelector } from 'react-redux';
import i18n from 'i18next';
import moment from 'moment';
import { Icon, Dropdown, Menu, Table, Tag, Row, Col, Select } from 'antd';
import { STATUS_EVENT } from '../../configs/localData';
import {
  deleteEventAction,
  getAllEventsAction,
  unSubscribeAction,
} from '../../redux/events/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';
import TableWrapper from '../../components/common/CustomTable/style';

const { Option } = Select;

const EventContainer = ({
  showModalWithItem,
  deleteEvent,
  getAllEvents,
  getPreviousEvents,
  getNextEvents,
  lastEvent,
  firstEvent,
  disableNext,
  disablePrev,
  unSubscribe,
  getImageUrl,
}) => {
  useEffect(() => {
    getAllEvents();
    return () => {
      unSubscribe();
    };
  }, [getAllEvents, unSubscribe]);
  const { events } = useSelector(state => state.events);
  console.log('asdasd events', events)
  const gotoEditPage = record => {
    const route = '/events/edit';
    showModalWithItem({ route, item: record });
  };
  const [filter, setFilter] = useState('All');

  const handleDelete = record => {
    deleteEvent({ id: record.id });
  };
  const menuItems = [
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage,
    },
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDelete,
    },
  ];
  const columns = [
    {
      title: i18n.t('table.title.name'),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: i18n.t('table.title.startDate'),
      dataIndex: 'startDate',
      render: time => <span>{moment(time).format('DD/MMM/YYYY')}</span>,
      key: 'startDate',
    },
    {
      title: i18n.t('table.title.endDate'),
      dataIndex: 'endDate',
      render: time => <span>{moment(time).format('DD/MMM/YYYY')}</span>,
      key: 'endDate',
    },
    {
      title: i18n.t('table.title.address'),
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: i18n.t('table.title.descriptions'),
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: i18n.t('table.title.status'),
      dataIndex: 'status',
      key: 'status',
      render: status => (
        <Tag
          color={
            STATUS_EVENT.find(item => item.value === status) &&
            STATUS_EVENT.find(item => item.value === status).color
          }
        >
          {i18n.t(
            STATUS_EVENT.find(item => item.value === status) &&
              STATUS_EVENT.find(item => item.value === status).text,
          )}
        </Tag>
      ),
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      fixed: 'right',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={
            <Menu>
              {menuItems.map(menuItem => (
                <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Icon type="more" />
        </Dropdown>
      ),
    },
  ];
  return (
    <>
      <TableWrapper>
        <Row gutter={10}>
          <Col span={2} style={{ paddingTop: '14px', width: '60px' }}>
            <span style={{ fontSize: '14px', color: '#1aa768' }}>Status</span>
          </Col>
          <Col span={7}>
            <Select
              onChange={e => setFilter(e)}
              showSearch
              defaultValue="All"
              style={{ width: 300, marginTop: '10px', marginBottom: '5px' }}
              placeholder="Filter Status"
            >
              <Option value="All">All</Option>
              <Option value={true}>Active</Option>
              <Option value={false}>Inactive</Option>
            </Select>
          </Col>
        </Row>
        <Table
          bordered
          pagination={{ pageSize: 4 }}
          columns={columns}
          scroll={{ x: 1300 }}
          dataSource={
            filter === 'All' ? events : events.filter(e => e.status === filter)
          }
        />
      </TableWrapper>
    </>
  );
};

const mapDispatchToProps = dispatch => ({
  deleteEvent: params => dispatch(deleteEventAction(params)),
  showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  getAllEvents: () => dispatch(getAllEventsAction()),
  unSubscribe: () => dispatch(unSubscribeAction()),
});

export default connect(null, mapDispatchToProps)(EventContainer);
