import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import PrivateLayout from '../../layout/PrivateLayout';
import { PageContent } from '../../components/common';
import { Archive, Userlyout, Calendar } from '../../assets/icons';
import ModalLayout from '../../layout/ModalLayout';
import { Donations } from '../../pages';
import Volunteer from '../../pages/Volunteer';
import Event from '../../pages/Events';

export const PRIVATE_ROUTES = [

  {
    key: 'donations',
    exact: true,
    text: 'Donations',
    icon: Archive,
    path: '/',
    customHeader: true,
    routes: [
      {
        path: '/',
        title: 'donation.title',
        component: Donations.List,
      },
    ],
    // modals: [
    //   {
    //     path: '/create',
    //     component: Donations.Create,
    //     title: 'donation.modals.create',
    //   },
    //   {
    //     path: '/edit',
    //     component: Donations.Edit,
    //     title: 'donation.modals.edit',
    //   },
    // ],
  },
  {
    key: 'volunteers',
    exact: true,
    text: 'Volunteers',
    icon: Userlyout,
    path: '/volunteers',
    routes: [
      {
        path: '/',
        title: 'volunteer.title',
        component: Volunteer.List,
      },
    ],
    modals: [
      {
        path: '/create',
        component: Volunteer.Create,
        title: 'employee.modals.create',
      },
      {
        path: '/edit',
        component: Volunteer.Edit,
        title: 'employee.modals.edit',
      },
      {
        path: '/successDonations',
        component: Volunteer.SuccessDonations,
        title: 'employee.modals.successDonations',
      },
    ],
  },
  {
    key: 'events',
    exact: true,
    text: 'Events',
    icon: Calendar,
    path: '/events',
    routes: [
      {
        path: '/',
        title: 'event.title',
        component: Event.List,
      },
    ],
    modals: [
      {
        path: '/create',
        component: Event.Create,
        title: 'event.modals.create',
      },
      {
        path: '/edit',
        component: Event.Edit,
        title: 'event.modals.edit',
      },
    ],
  },
];

export const MODAL_LISTS = [...PRIVATE_ROUTES].reverse(({ path, modals }) => ({
  path,
  modals,
}));

const PrivateRoutes = () => (
  <PrivateLayout>
    <ModalLayout />
    <Switch>
      {PRIVATE_ROUTES.flatMap(
        route =>
          route.routes &&
          route.routes.map(subRoute => ({
            ...route,
            ...subRoute,
            path: route.path + subRoute.path,
            exact: subRoute.path === '/',
          })),
      ).map(route => (
        <Route
          {...route}
          component={e => (
            <>
              {!route.customHeader && <PageContent title={route.title} {...e} path={route.path} />}
              <route.component {...e} />
            </>
          )}
          key={route.path}
        />
      ))}
      <Redirect to="/" />
    </Switch>
  </PrivateLayout>
);

export default PrivateRoutes;
