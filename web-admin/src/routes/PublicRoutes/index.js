import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from '../../pages/Login';
import { NotFoundPage } from '../../pages';

export const PUBLIC_ROUTES = [
  {
    path: '/login',
    component: Login
  }
];

const PublicRoutes = () => (
  <Switch>
    {PUBLIC_ROUTES.flatMap(route =>
      route.routes
        ? route.routes.map(subRoute => ({
            ...subRoute,
            path: route.path + subRoute.path,
            exact: subRoute.path === '/'
          }))
        : route
    ).map(route => (
      <Route {...route} key={route.path} />
    ))}
    {/* <Route component={NotFoundPage} /> */}
  </Switch>
);

export default PublicRoutes;
