export const FORMAT_DATE = 'MMM DD, YYYY';

export const GENDER = [
  {
    value: 'male',
    text: 'gender.male',
  },
  {
    value: 'female',
    text: 'gender.female',
  },
];
export const STATUS = [
  {
    value: 'true',
    text: 'status.active',
  },
  {
    value: 'false',
    text: 'status.inactive',
  },
];

export const STATUS_EVENT = [
  {
    value: true,
    color: 'green',
    text: 'status.active',
  },
  {
    value: false,
    color: 'red',
    text: 'status.inactive',
  },
];
