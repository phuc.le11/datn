import Create from './Create';
import Edit from './Edit';
import { VolunteerTable as List } from '../../containers';
import SuccessDonations from './SuccessDonations';

export default { Create, Edit, List, SuccessDonations };
