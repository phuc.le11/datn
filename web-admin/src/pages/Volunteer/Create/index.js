import React, { useEffect } from 'react';
import { Form, Button, Input } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { draftSelector } from '../../../redux/modal/selectors';
import { createVolunteerAction } from '../../../redux/volunteers/actions';
import CustomDatePicker from '../../../components/common/CustomDatePicker';

const Create = ({
  form,
  createVolunteer,
  closeModal,
  id,
  saveDraft,
  draft,
}) => {
  const {
    validateFields,
    getFieldDecorator,
    setFieldsValue,
    getFieldsValue,
  } = form;
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        createVolunteer({
          volunteer: {
            name: values.name,
            email: values.email,
            birthDay: new Date(values.birthDay).getTime(),
            address: values.address,
            phoneNumber: values.phoneNumber,
          },
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);

  return (
    <Form onSubmit={onSubmit}>
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator('name', {
          rules: [{ required: true, message: 'Please enter email' }],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator('email', {
          rules: [{ required: true, message: 'Please enter email' }],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="Address" colon={false}>
        {getFieldDecorator('address', {
          rules: [
            {
              required: true,
              message: 'Please enter address',
            },
          ],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="Phone Number" colon={false}>
        {getFieldDecorator('phoneNumber', {
          rules: [{ required: true, message: 'Please enter Number Phone' }],
        })(<Input />)}
      </Form.Item>
      <CustomDatePicker
        form={form}
        label="BirthDay"
        colon={false}
        src="birthDay"
        required
        message="Please enter date"
      />
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.create')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Create.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  createVolunteer: PropTypes.func,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  id: PropTypes.number,
};

Create.defaultProps = {};

const mapStateToProps = (state, props) => ({
  draft: draftSelector(state, props),
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  createVolunteer: params => dispatch(createVolunteerAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(Create));
