import React from 'react';
import { List } from 'antd';
import { connect } from 'react-redux';
import { itemSelector, draftSelector } from '../../../redux/modal/selectors';
// import PropTypes from 'prop-types'

const SuccessDonations = ({ recordData }) => {
  return (
    <List
      itemLayout="horizontal"
      dataSource={recordData}
      renderItem={item => (
        <List.Item>
          <List.Item.Meta
            title={<a href="https://ant.design">{item.name}</a>}
            description="Ant Design, a design language for background applications, is refined by Ant UED Team"
          />
        </List.Item>
      )}
    />
  );
};

SuccessDonations.propTypes = {};

const mapStateToProps = (state, props) => ({
  recordData: itemSelector(state, props),
  draft: draftSelector(state, props),
});

const mapDispatchToProps = dispatch => ({});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SuccessDonations);
