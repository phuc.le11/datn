import React, { useEffect } from 'react';
import { Form, Button, Input, Switch, InputNumber } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { draftSelector } from '../../../redux/modal/selectors';
import { createEventAction } from '../../../redux/events/actions';
import CustomDatePicker from '../../../components/common/CustomDatePicker';

const Create = ({ form, createEvent, closeModal, id, saveDraft, draft }) => {
  const {
    validateFields,
    getFieldDecorator,
    setFieldsValue,
    getFieldsValue,
  } = form;
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        createEvent({
          event: {
            stt: values.stt,
            name: values.name,
            startDate: values.startDate.toISOString(),
            endDate: values.endDate.toISOString(),
            address: values.address,
            status: values.status,
            description: values.description,
          },
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);

  return (
    <Form onSubmit={onSubmit}>
      <Form.Item label="Number Order" colon={false}>
        {getFieldDecorator('stt', {
          rules: [{ required: true, message: 'Please enter Number Order' }],
        })(<InputNumber />)}
      </Form.Item>
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator('name', {
          rules: [{ required: true, message: 'Please enter name' }],
        })(<Input min={1} />)}
      </Form.Item>
      <Form.Item label="Address" colon={false}>
        {getFieldDecorator('address', {
          rules: [
            {
              required: true,
              message: 'Please enter address',
            },
          ],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="Description" colon={false}>
        {getFieldDecorator('description', {
          rules: [
            {
              required: true,
              message: 'Please enter description',
            },
          ],
        })(<Input.TextArea />)}
      </Form.Item>
      <CustomDatePicker
        form={form}
        label="Start Day"
        colon={false}
        src="startDate"
        required
        message="Please enter date"
      />
      <CustomDatePicker
        form={form}
        label="End Day"
        colon={false}
        src="endDate"
        required
        message="Please enter date"
      />
      <Form.Item label="Status" colon={false}>
        {getFieldDecorator('status', {
          rules: [
            {
              required: true,
              message: 'Please set status',
            },
          ],
        })(<Switch />)}
      </Form.Item>
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.create')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Create.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  createEvent: PropTypes.func,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  id: PropTypes.number,
};

Create.defaultProps = {};

const mapStateToProps = (state, props) => ({
  draft: draftSelector(state, props),
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  createEvent: params => dispatch(createEventAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(Create));
