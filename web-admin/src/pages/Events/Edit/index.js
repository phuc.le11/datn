import React, { useEffect } from 'react';
import { Form, Button, Input, DatePicker, Switch, InputNumber } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { editEventAction } from '../../../redux/events/actions';
import { itemSelector, draftSelector } from '../../../redux/modal/selectors';

const Edit = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  recordData,
  editEvent,
  id,
  draft,
  closeModal,
  saveDraft,
}) => {
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        // const birthDay = new Date(values.birthDay).getTime();
        editEvent({
          id: recordData.id,
          event: {
            stt: values.stt,
            name: values.name,
            address: values.address,
            startDate: values.startDate.toISOString(),
            endDate: values.endDate.toISOString(),
            status: values.status,
            description: values.description,
          },
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  return (
    <Form onSubmit={onSubmit} className="login-form">
      <Form.Item label="Number Order" colon={false}>
        {getFieldDecorator(
          'stt',
          {
            initialValue: recordData.stt || null,
          },
          {
            rules: [{ required: true, message: 'Please enter Number Order' }],
          },
        )(<InputNumber style={{ color: 'rgba(0,0,0,.25)' }} disabled />)}
      </Form.Item>
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator(
          'name',
          {
            initialValue: recordData.name || null,
          },
          {
            rules: [{ required: true, message: 'Please enter name' }],
          },
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Date" colon={false}>
        {getFieldDecorator(
          'startDate',
          {
            initialValue: moment(recordData.startDate),
          },
          {
            rules: [
              {
                required: true,
                message: i18n.t('messages.date'),
              },
            ],
          },
        )(<DatePicker />)}
      </Form.Item>
      <Form.Item label="Date" colon={false}>
        {getFieldDecorator(
          'endDate',
          {
            initialValue: moment(recordData.endDate),
          },
          {
            rules: [
              {
                required: true,
                message: i18n.t('messages.date'),
              },
            ],
          },
        )(<DatePicker />)}
      </Form.Item>
      <Form.Item label="Address" colon={false}>
        {getFieldDecorator(
          'address',
          {
            initialValue: recordData.address || null,
          },
          {
            rules: [{ required: true, message: 'Please enter address' }],
          },
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Description" colon={false}>
        {getFieldDecorator(
          'description',
          {
            initialValue: recordData.description || null,
          },
          {
            rules: [{ required: true, message: 'Please enter description' }],
          },
        )(<Input.TextArea />)}
      </Form.Item>
      <Form.Item label="Status" colon={false}>
        {getFieldDecorator('status', {
          rules: [
            {
              required: true,
              message: 'Please set status',
            },
          ],
        })(<Switch defaultChecked={recordData.status} />)}
      </Form.Item>
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.edit')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Edit.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  editVolunteer: PropTypes.func,
  id: PropTypes.number,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  recordData: PropTypes.object,
};

const mapStateToProps = (state, props) => ({
  recordData: itemSelector(state, props),
  draft: draftSelector(state, props),
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  editEvent: params => dispatch(editEventAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(Edit));
