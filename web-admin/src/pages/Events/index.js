import Create from './Create';
import Edit from './Edit';
import { EventTable as List } from '../../containers';

export default { Create, Edit, List };
