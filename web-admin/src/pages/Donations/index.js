import Create from './Create';
import Edit from './Edit';
import { DonationsTable as List } from '../../containers';

export default { Create, Edit, List };
