import React, { useEffect } from 'react';
import { Form, Button, Input, DatePicker, Select } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { uploadImageAction } from '../../../redux/firebaseImg/actions';
import { itemSelector, draftSelector } from '../../../redux/modal/selectors';
import { getAllDonationsAction } from '../../../redux/donations/actions';

const Edit = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  recordData,
  editEmployee,
  id,
  draft,
  closeModal,
  saveDraft,
  uploadImage,
  volunteers,
  getAllVolunteers,
}) => {
  const nameVolunteer =
    volunteers && volunteers.find(item => item.id === recordData.volunteerId);
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        editEmployee({
          id: recordData.id,
          employee: {
            giverName: values.giverName,
            email: values.email,
            address: values.address,
            phoneNumber: values.phoneNumber,
            volunteerId: nameVolunteer.id,
            donationTime: moment(values.donationTime),
            // imgName: values.profilePic.file
            //   ? values.profilePic.file.uid
            //   : values.profilePic.uid,
          },
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    getAllVolunteers();
  }, [getAllVolunteers]);
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);

  // const handleUpload = ({ onError, onSuccess, file }) => {
  //   try {
  //     uploadImage(file, onSuccess);
  //   } catch (error) {
  //     onError(error);
  //   }
  // };
  return (
    <Form onSubmit={onSubmit} className="login-form">
      <Form.Item label="GiverName" colon={false}>
        {getFieldDecorator(
          'giverName',
          {
            initialValue: recordData.giverName || null,
          },
          {
            rules: [{ required: true, message: 'Please enter giver name' }],
          },
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Address" colon={false}>
        {getFieldDecorator(
          'address',
          {
            initialValue: recordData.address || null,
          },
          {
            rules: [{ required: true, message: 'Please enter address' }],
          },
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="PhoneNumber" colon={false}>
        {getFieldDecorator(
          'phoneNumber',
          {
            initialValue: recordData.phoneNumber,
          },
          {
            rules: [
              {
                required: true,
                message: 'Please enter giver phone number',
              },
            ],
          },
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Volunteer" colon={false}>
        {getFieldDecorator('Volunteers', {
          initialValue: nameVolunteer.name,
        })(
          <Select>
            {volunteers.map(volunteer => (
              <Select.Option key={volunteer.id} value={volunteer.name}>
                {volunteer.name}
              </Select.Option>
            ))}
          </Select>,
        )}
      </Form.Item>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator('email', {
          initialValue: recordData.email || null,
        })(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>

      <Form.Item label="Giver Date" colon={false}>
        {getFieldDecorator(
          'donationTime',
          {
            initialValue: moment(recordData.donationTime),
          },
          {
            rules: [
              {
                required: true,
                message: i18n.t('messages.date'),
              },
            ],
          },
        )(<DatePicker />)}
      </Form.Item>
      {/* <Form.Item label="Avatar" colon={false}>
        {getFieldDecorator('profilePic', {
          initialValue: {
            status: 'done',
            uid: recordData.imgName,
          },
        })(
          <Upload name="logo" customRequest={handleUpload} listType="picture">
            <Button>
              <Icon type="upload" />
            </Button>
          </Upload>,
        )}
      </Form.Item> */}
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.edit')}
        </Button>
      </Form.Item>
    </Form>
  );
};

const mapStateToProps = (state, props) => ({
  recordData: itemSelector(state, props),
  draft: draftSelector(state, props),
  volunteers: state.volunteers.volunteers,
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  saveDraft: params => dispatch(saveDraftAction(params)),
  uploadImage: (file, onSuccess) =>
    dispatch(uploadImageAction(file, onSuccess)),
  getAllVolunteers: () => dispatch(getAllDonationsAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(Edit));
