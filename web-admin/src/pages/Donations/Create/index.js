import React, { useEffect } from 'react';
import { Form, Button, Input, Select } from 'antd';
import i18n from 'i18next';
import { connect, useSelector } from 'react-redux';
import {
  closeModalAction,
  saveDraftAction
} from '../../../redux/modal/actions';
import { draftSelector } from '../../../redux/modal/selectors';
import { createDonationAction } from '../../../redux/donations/actions';
import { CustomUpload } from '../../../containers/common';
import CustomDatePicker from '../../../components/common/CustomDatePicker';
import { getAllVolunteersAction } from '../../../redux/volunteers/actions';

const { Option } = Select;

const Create = ({
  getAllVolunteers,
  form,
  createDonation,
  employees,
  closeModal,
  id,
  saveDraft,
  getAllEmployee,
  draft
}) => {
  const {
    validateFields,
    getFieldDecorator,
    setFieldsValue,
    getFieldsValue
  } = form;
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        createDonation({
          donation: {
            name: values.name,
            job: values.job,
            email: values.email,
            birthday: new Date(values.birthday).getTime(),
            dayInCompany: new Date(values.dayInCompany).getTime(),
            imgName: values.imgURL.file.uid
          }
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);

  useEffect(() => {
    getAllVolunteers();
  }, [getAllVolunteers]);
  const volunteers = useSelector(state => state.volunteers.volunteers);
  return (
    <Form onSubmit={onSubmit}>
      <Form.Item label="Employee Id" colon={false}>
        {getFieldDecorator('id', {
          rules: [{ required: true, message: 'Please enter email' }]
        })(<Input />)}
      </Form.Item>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator('email', {
          rules: [{ required: true, message: 'Please enter email' }]
        })(<Input />)}
      </Form.Item>
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator('name', {
          rules: [{ required: true, message: 'Please enter email' }]
        })(<Input />)}
      </Form.Item>
      <CustomDatePicker
        form={form}
        label="Start Work Date"
        colon={false}
        src="dayInCompany"
        required
        message="Please enter date"
      />
      <CustomDatePicker
        form={form}
        label="BirthDay"
        colon={false}
        src="birthday"
        required
        message="Please enter date"
      />
      <Form.Item label="Position" colon={false}>
        {getFieldDecorator('job', {
          rules: [
            {
              required: true,
              message: 'Please select your job!'
            }
          ]
        })(
          <Select>
            {volunteers &&
              volunteers.map(volunteer => (
                <Option key={volunteer.id} value={volunteer.name}>
                  {volunteer.name}
                </Option>
              ))}
          </Select>
        )}
      </Form.Item>
      <CustomUpload
        form={form}
        label="Avatar"
        src="imgURL"
        colon={false}
        message="Please up your image"
        required
      />
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.create')}
        </Button>
      </Form.Item>
    </Form>
  );
};

const mapStateToProps = (state, props) => ({
  draft: draftSelector(state, props)
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  createDonation: params => dispatch(createDonationAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
  getAllVolunteers: () => dispatch(getAllVolunteersAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(Create));
